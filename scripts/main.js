var filmovi = [{"idFilma": "1",
            "naziv": "A Bad Mom's Christmas",
			"poster": "../data/imgs/posteri/A Bad Mom's Christmas.jpg",
			"zanr": "comedy",
			"yturl": "https://www.youtube.com/embed/EQPr0sCjau0?ecver=1",
			"trajanje": "01|44"},
			{"idFilma": "2",
            "naziv": "Star Wars - The Last Jedi",
			"poster": "../data/imgs/posteri/Star Wars - The Last Jedi.jpg",
			"zanr": "fantasy",
			"yturl": "https://www.youtube.com/embed/Q0CbN8sfihY?ecver=1",
			"trajanje": "02|32"},
			{"idFilma": "3",
            "naziv": "The Godfather",
			"poster": "../data/imgs/posteri/The Godfather.jpg",
			"zanr": "drama",
			"yturl": "https://www.youtube.com/embed/sY1S34973zA?ecver=1",
			"trajanje": "02|58"},
			{"idFilma": "4",
            "naziv": "Titanic",
			"poster": "../data/imgs/posteri/Titanic.jpg",
			"zanr": "drama",
			"yturl": "https://www.youtube.com/embed/ZQ6klONCq4s?ecver=1",
			"trajanje": "03|14"}]

var zanrovi = ["comedy", "fantasy", "drama", "adventure"]

var projekcije = [{"idProjekcije": "1",
	            "pocetakProjekcije": "2018|03|27|15|45",
	            "duzinaProjekcije": "01|44",
	        	"cena": "300",
	        	"nazivFilma": "A Bad Mom's Christmas",
	        	"trailerZaFilm": "https://www.youtube.com/embed/EQPr0sCjau0?ecver=1",
	        	"zanrFilma": "comedy",
	        	"obrisana": false,
	        	"sala": "A1",
	        	"brojSlobodnihMesta": "200",
	        	"brojMestaUSali": "200"},
				{"idProjekcije": "2",
	            "pocetakProjekcije": "2018|05|30|05|20",
	            "duzinaProjekcije": "02|32",
	        	"cena": "350",
	        	"nazivFilma": "Star Wars - The Last Jedi",
	        	"trailerZaFilm": "https://www.youtube.com/embed/Q0CbN8sfihY?ecver=1",
	        	"zanrFilma": "fantasy",
	        	"obrisana": false,
	        	"sala": "A2",
	        	"brojSlobodnihMesta": "150",
	        	"brojMestaUSali": "150"},
				{"idProjekcije": "3",
	            "pocetakProjekcije": "2018|01|01|05|20",
	            "duzinaProjekcije": "02|32",
	        	"cena": "350",
	        	"nazivFilma": "Star Wars - The Last Jedi",
	        	"trailerZaFilm": "https://www.youtube.com/embed/Q0CbN8sfihY?ecver=1",
	        	"zanrFilma": "fantasy",
	        	"obrisana": false,
	        	"sala": "A2",
	        	"brojSlobodnihMesta": "150",
	        	"brojMestaUSali": "150"}]

var sale = [{"idSale": "A1",
			"brojMestaUSali": "200"},
			{"idSale": "A2",
			"brojMestaUSali": "150"},
			{"idSale": "B1",
			"brojMestaUSali": "200"},
			{"idSale": "B2",
			"brojMestaUSali": "250"}]

function upisiULocalStorageFilmove() {
	if (typeof(Storage) !== "undefined") {
		var postojanjeFilmovi = localStorage.getItem(filmovi);
		if (!postojanjeFilmovi) {
			localStorage.setItem("filmovi", JSON.stringify(filmovi));
		}
		var postojanjeZanrovi = localStorage.getItem(zanrovi);
		if (!postojanjeZanrovi) {
			localStorage.setItem("zanrovi", JSON.stringify(zanrovi));
		}
	}
}

function prikaziVise() {
    var x = document.getElementById("opcijeZaProjekcije");

    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }	
}

function prikaziVisePretragaProjekcija() {
    var x = document.getElementById("opcijeZaPromenuProjekcije");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }	
}

function ukloniMeni() {
	var x = document.getElementById("drugiRedLevoMeni");
	x.removeChild(x.childNodes[0]);
	/*console.log(x.childNodes[0]);
	console.log(isMeniClicked);*/
	isMeniClicked = false;
	return isMeniClicked
} 


var isMeniClicked = false;
function stvori() {
	if(isMeniClicked === false){
		var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
		aktivniKorisnik = JSON.parse(aktivniKorisnik);
		if (aktivniKorisnik["uloga"] == "menadzer") {
			var div = document.getElementById("drugiRedLevoMeni");
			var br = document.createElement("br");
			var fieldset = document.createElement("fieldset");
			fieldset.setAttribute("style", "width:75%");
			fieldset.setAttribute("id", "fieldsetMeni");
			fieldset.setAttribute("style", "border-radius:10px;");
			var legenda = document.createElement("legend");
			legenda.setAttribute("style", "font-weight:bold")
			var tekstlegenda = document.createTextNode(" Meni: ");
			legenda.appendChild(tekstlegenda);
			var Projekcije = document.createElement("p");
			var tekstProjekcije = document.createTextNode(" Projekcije ");
			Projekcije.appendChild(tekstProjekcije);
			Projekcije.setAttribute("onclick", "prikaziVise()");
			Projekcije.setAttribute("style", "cursor:pointer");
			var listaProjekcije = document.createElement("ul");
			listaProjekcije.setAttribute("id", "opcijeZaProjekcije");
			listaProjekcije.setAttribute("style", "display:none");
			var listaPretragaProjekcija = document.createElement("ul");
			listaPretragaProjekcija.setAttribute("id", "opcijeZaPromenuProjekcije");
			listaPretragaProjekcija.setAttribute("style", "display:none");
			var PretragaProjekcija = document.createElement("li");
			var PretragaProjekcijaParagraf = document.createElement("p");
			var tekstPretragaProjekcija = document.createTextNode(" Pretraga Projekcija ");
			PretragaProjekcijaParagraf.setAttribute("style", "cursor:pointer");
			PretragaProjekcijaParagraf.appendChild(tekstPretragaProjekcija);
			PretragaProjekcija.appendChild(PretragaProjekcijaParagraf);
			PretragaProjekcijaParagraf.setAttribute("onclick", "prikaziVisePretragaProjekcija()");
			var UnosNoveProjekcije = document.createElement("li");
			var UnosNoveProjekcijeParagraf = document.createElement("p");
			var tekstUnosNoveProjekcije = document.createTextNode(" Unos nove projekcije ");
			UnosNoveProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			UnosNoveProjekcijeParagraf.setAttribute("onclick", "unosNoveProjekcije()");
			UnosNoveProjekcijeParagraf.appendChild(tekstUnosNoveProjekcije);
			UnosNoveProjekcije.appendChild(UnosNoveProjekcijeParagraf);
			var BrisanjeProjekcije = document.createElement("li");
			var BrisanjeProjekcijeParagraf = document.createElement("p");
			var tekstBrisanjeProjekcije = document.createTextNode(" Brisanje projekcije ");
			BrisanjeProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			BrisanjeProjekcijeParagraf.setAttribute("onclick", "brisanjeIIzmenaProjekcije()");
			BrisanjeProjekcijeParagraf.appendChild(tekstBrisanjeProjekcije);
			BrisanjeProjekcije.appendChild(BrisanjeProjekcijeParagraf);
			var IzmenaProjekcije = document.createElement("li");
			var IzmenaProjekcijeParagraf = document.createElement("p");
			var tekstIzmenaProjekcije = document.createTextNode(" Izmena projekcije ");
			IzmenaProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			IzmenaProjekcijeParagraf.setAttribute("onclick", "brisanjeIIzmenaProjekcije()");
			IzmenaProjekcijeParagraf.appendChild(tekstIzmenaProjekcije);
			IzmenaProjekcije.appendChild(IzmenaProjekcijeParagraf);
			listaProjekcije.appendChild(PretragaProjekcija);
			PretragaProjekcija.appendChild(listaPretragaProjekcija);
			listaProjekcije.appendChild(UnosNoveProjekcije);
			listaProjekcije.appendChild(BrisanjeProjekcije);
			listaProjekcije.appendChild(IzmenaProjekcije);
			var PretragaPremaIDju = document.createElement("li");
			var tekstPretragaPremaIDju = document.createTextNode(" Pretraga prema ID-ju ");
			PretragaPremaIDju.appendChild(tekstPretragaPremaIDju);
			PretragaPremaIDju.setAttribute("style", "cursor:pointer");
			PretragaPremaIDju.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaNazivuFilma = document.createElement("li");
			var tekstPretragaPremaNazivuFilma = document.createTextNode(" Pretraga prema nazivu filma ");
			PretragaPremaNazivuFilma.appendChild(tekstPretragaPremaNazivuFilma);
			PretragaPremaNazivuFilma.setAttribute("style", "cursor:pointer");
			PretragaPremaNazivuFilma.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaŽanruFilma = document.createElement("li");
			var tekstPretragaPremaŽanruFilma = document.createTextNode(" Pretraga prema žanru filma ");
			PretragaPremaŽanruFilma.appendChild(tekstPretragaPremaŽanruFilma);
			PretragaPremaŽanruFilma.setAttribute("style", "cursor:pointer");
			PretragaPremaŽanruFilma.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaSali = document.createElement("li");
			var tekstPretragaPremaSali = document.createTextNode(" Pretraga prema sali ");
			PretragaPremaSali.appendChild(tekstPretragaPremaSali);
			PretragaPremaSali.setAttribute("style", "cursor:pointer");
			PretragaPremaSali.setAttribute("onclick", "prikazPretrage()");
			listaPretragaProjekcija.appendChild(PretragaPremaIDju);
			listaPretragaProjekcija.appendChild(PretragaPremaNazivuFilma);
			listaPretragaProjekcija.appendChild(PretragaPremaŽanruFilma);
			listaPretragaProjekcija.appendChild(PretragaPremaSali);
			var DodavanjeProdavca = document.createElement("p");
			var tekstDodavanjeProdavca = document.createTextNode(" Dodavanje prodavca ");
			DodavanjeProdavca.appendChild(tekstDodavanjeProdavca);
			DodavanjeProdavca.setAttribute("style", "cursor:pointer");
			DodavanjeProdavca.setAttribute("id", "dodavanjeProdavca");
			DodavanjeProdavca.setAttribute("onclick", "prikaziFormuZaDodavanjeNovogProdavca()");
			var ProdajaKarata = document.createElement("p");
			var tekstProdajaKarata = document.createTextNode(" Prodaja karata ");
			ProdajaKarata.appendChild(tekstProdajaKarata);
			ProdajaKarata.setAttribute("style", "cursor:pointer");
			var ZatvoriMeni = document.createElement("p");
			ZatvoriMeni.setAttribute("onclick", "ukloniMeni()");
			ZatvoriMeni.setAttribute("style", "cursor:pointer");
			var tekstZatvoriMeni = document.createTextNode(" Zatvori meni ");
			ZatvoriMeni.appendChild(tekstZatvoriMeni);

			var UnesiNoviFilm = document.createElement("p");
			UnesiNoviFilm.setAttribute("onclick", "prikaziFormuZaDodavanjeNovogFilma()");
			UnesiNoviFilm.setAttribute("style", "cursor:pointer");
			var tekstUnesiNoviFilm = document.createTextNode(" Unos novog filma ");
			UnesiNoviFilm.appendChild(tekstUnesiNoviFilm);

			var PodaciOAktivnomKorisniku = document.createElement("p");
			PodaciOAktivnomKorisniku.setAttribute("onclick", "PodaciOAktivnomKorisniku()");
			PodaciOAktivnomKorisniku.setAttribute("style", "cursor:pointer");
			var tekstPodaciOAktivnomKorisniku = document.createTextNode(" Podaci o trenutnom korisniku ");
			PodaciOAktivnomKorisniku.appendChild(tekstPodaciOAktivnomKorisniku);
			fieldset.appendChild(legenda);
			fieldset.appendChild(Projekcije);
			fieldset.appendChild(listaProjekcije);
			fieldset.appendChild(DodavanjeProdavca);
			/*fieldset.appendChild(UnesiNoviFilm);*/
			/*fieldset.appendChild(ProdajaKarata);*/
			/*fieldset.appendChild(PodaciOAktivnomKorisniku);*/
			fieldset.appendChild(ZatvoriMeni);
			div.appendChild(fieldset);
		} else{
			var div = document.getElementById("drugiRedLevoMeni");
			var br = document.createElement("br");
			var fieldset = document.createElement("fieldset");
			fieldset.setAttribute("style", "width:75%");
			fieldset.setAttribute("id", "fieldsetMeni");
			fieldset.setAttribute("style", "border-radius:10px;");
			var legenda = document.createElement("legend");
			legenda.setAttribute("style", "font-weight:bold")
			var tekstlegenda = document.createTextNode(" Meni: ");
			legenda.appendChild(tekstlegenda);
			var Projekcije = document.createElement("p");
			var tekstProjekcije = document.createTextNode(" Projekcije ");
			Projekcije.appendChild(tekstProjekcije);
			Projekcije.setAttribute("onclick", "prikaziVise()");
			Projekcije.setAttribute("style", "cursor:pointer");
			var listaProjekcije = document.createElement("ul");
			listaProjekcije.setAttribute("id", "opcijeZaProjekcije");
			listaProjekcije.setAttribute("style", "display:none");
			var listaPretragaProjekcija = document.createElement("ul");
			listaPretragaProjekcija.setAttribute("id", "opcijeZaPromenuProjekcije");
			listaPretragaProjekcija.setAttribute("style", "display:none");
			var PretragaProjekcija = document.createElement("li");
			var PretragaProjekcijaParagraf = document.createElement("p");
			var tekstPretragaProjekcija = document.createTextNode(" Pretraga Projekcija ");
			PretragaProjekcijaParagraf.setAttribute("style", "cursor:pointer");
			PretragaProjekcijaParagraf.appendChild(tekstPretragaProjekcija);
			PretragaProjekcija.appendChild(PretragaProjekcijaParagraf);
			PretragaProjekcijaParagraf.setAttribute("onclick", "prikaziVisePretragaProjekcija()");
			var UnosNoveProjekcije = document.createElement("li");
			var UnosNoveProjekcijeParagraf = document.createElement("p");
			var tekstUnosNoveProjekcije = document.createTextNode(" Unos nove projekcije ");
			UnosNoveProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			UnosNoveProjekcijeParagraf.appendChild(tekstUnosNoveProjekcije);
			UnosNoveProjekcije.appendChild(UnosNoveProjekcijeParagraf);
			var BrisanjeProjekcije = document.createElement("li");
			var BrisanjeProjekcijeParagraf = document.createElement("p");
			var tekstBrisanjeProjekcije = document.createTextNode(" Brisanje projekcije ");
			BrisanjeProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			BrisanjeProjekcijeParagraf.appendChild(tekstBrisanjeProjekcije);
			BrisanjeProjekcije.appendChild(BrisanjeProjekcijeParagraf);
			var IzmenaProjekcije = document.createElement("li");
			var IzmenaProjekcijeParagraf = document.createElement("p");
			var tekstIzmenaProjekcije = document.createTextNode(" Izmena projekcije ");
			IzmenaProjekcijeParagraf.setAttribute("style", "cursor:pointer");
			IzmenaProjekcijeParagraf.appendChild(tekstIzmenaProjekcije);
			IzmenaProjekcije.appendChild(IzmenaProjekcijeParagraf);
			listaProjekcije.appendChild(PretragaProjekcija);
			PretragaProjekcija.appendChild(listaPretragaProjekcija);
			/*listaProjekcije.appendChild(UnosNoveProjekcije);
			listaProjekcije.appendChild(BrisanjeProjekcije);
			listaProjekcije.appendChild(IzmenaProjekcije);*/
			var PretragaPremaIDju = document.createElement("li");
			var tekstPretragaPremaIDju = document.createTextNode(" Pretraga prema ID-ju ");
			PretragaPremaIDju.appendChild(tekstPretragaPremaIDju);
			PretragaPremaIDju.setAttribute("style", "cursor:pointer");
			PretragaPremaIDju.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaNazivuFilma = document.createElement("li");
			var tekstPretragaPremaNazivuFilma = document.createTextNode(" Pretraga prema nazivu filma ");
			PretragaPremaNazivuFilma.appendChild(tekstPretragaPremaNazivuFilma);
			PretragaPremaNazivuFilma.setAttribute("style", "cursor:pointer");
			PretragaPremaNazivuFilma.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaŽanruFilma = document.createElement("li");
			var tekstPretragaPremaŽanruFilma = document.createTextNode(" Pretraga prema žanru filma ");
			PretragaPremaŽanruFilma.appendChild(tekstPretragaPremaŽanruFilma);
			PretragaPremaŽanruFilma.setAttribute("style", "cursor:pointer");
			PretragaPremaŽanruFilma.setAttribute("onclick", "prikazPretrage()");
			var PretragaPremaSali = document.createElement("li");
			var tekstPretragaPremaSali = document.createTextNode(" Pretraga prema sali ");
			PretragaPremaSali.appendChild(tekstPretragaPremaSali);
			PretragaPremaSali.setAttribute("style", "cursor:pointer");
			PretragaPremaSali.setAttribute("onclick", "prikazPretrage()");
			listaPretragaProjekcija.appendChild(PretragaPremaIDju);
			listaPretragaProjekcija.appendChild(PretragaPremaNazivuFilma);
			listaPretragaProjekcija.appendChild(PretragaPremaŽanruFilma);
			listaPretragaProjekcija.appendChild(PretragaPremaSali);
			var DodavanjeProdavca = document.createElement("p");
			var tekstDodavanjeProdavca = document.createTextNode(" Dodavanje prodavca ");
			DodavanjeProdavca.appendChild(tekstDodavanjeProdavca);
			DodavanjeProdavca.setAttribute("style", "cursor:pointer");
			DodavanjeProdavca.setAttribute("id", "dodavanjeProdavca");
			DodavanjeProdavca.setAttribute("onclick", "prikaziFormuZaDodavanjeNovogProdavca()");
			var ProdajaKarata = document.createElement("p");
			var tekstProdajaKarata = document.createTextNode(" Prodaja karata ");
			ProdajaKarata.appendChild(tekstProdajaKarata);
			ProdajaKarata.setAttribute("style", "cursor:pointer");
			ProdajaKarata.setAttribute("onclick", "prodajaKarata()");
			var ZatvoriMeni = document.createElement("p");
			ZatvoriMeni.setAttribute("onclick", "ukloniMeni()");
			ZatvoriMeni.setAttribute("style", "cursor:pointer");
			var tekstZatvoriMeni = document.createTextNode(" Zatvori meni ");
			ZatvoriMeni.appendChild(tekstZatvoriMeni);
			var PodaciOAktivnomKorisniku = document.createElement("p");
			PodaciOAktivnomKorisniku.setAttribute("onclick", "PodaciOAktivnomKorisniku()");
			PodaciOAktivnomKorisniku.setAttribute("style", "cursor:pointer");
			var tekstPodaciOAktivnomKorisniku = document.createTextNode(" Podaci o trenutnom korisniku ");
			PodaciOAktivnomKorisniku.appendChild(tekstPodaciOAktivnomKorisniku);
			fieldset.appendChild(legenda);
			fieldset.appendChild(Projekcije);
			fieldset.appendChild(listaProjekcije);
			/*fieldset.appendChild(DodavanjeProdavca);*/
			fieldset.appendChild(ProdajaKarata);
			/*fieldset.appendChild(PodaciOAktivnomKorisniku);*/
			fieldset.appendChild(ZatvoriMeni);
			div.appendChild(fieldset);
		}
	}
	isMeniClicked = true;
	return isMeniClicked, DodavanjeProdavca;
}

function prodajaKarata() {
	window.location.replace("prodajaKarata.html");
}

function postaviKaoGlavnuSliku1() {
	var glavnaSlika = document.getElementById("prviRedLevo").children[0];
	var glavnaSlikaSrc = document.getElementById("prviRedLevo").children[0].src;
	var glavnaSlikaAlt = document.getElementById("prviRedLevo").children[0].alt;
	var glavniTekst = document.getElementById("prviRedTekstParagraf").children[0].innerHTML;
	var sporednaSlika = document.getElementById("prviRedDesno1").children[0];
	var sporednaSlikaSrc = document.getElementById("prviRedDesno1").children[0].src;
	var sporednaSlikaAlt = document.getElementById("prviRedDesno1").children[0].alt;
	var sporedniTekst = document.getElementById("prviRedDesno1").children[1].innerHTML;
	glavnaSlika.setAttribute("src", sporednaSlikaSrc);
	sporednaSlika.setAttribute("src", glavnaSlikaSrc);
	glavnaSlika.setAttribute("alt", sporednaSlikaAlt);
	sporednaSlika.setAttribute("alt", glavnaSlikaAlt);
	document.getElementById("prviRedTekstParagraf").children[0].innerHTML = sporedniTekst;
	document.getElementById("prviRedDesno1").children[1].innerHTML = glavniTekst;
	document.getElementById("prviRedTekstNaslovh1").innerHTML = sporednaSlikaAlt;
	/*console.log(glavniTekst);*/
}

function postaviKaoGlavnuSliku2() {
	var glavnaSlika = document.getElementById("prviRedLevo").children[0];
	var glavnaSlikaSrc = document.getElementById("prviRedLevo").children[0].src;
	var glavnaSlikaAlt = document.getElementById("prviRedLevo").children[0].alt;
	var glavniTekst = document.getElementById("prviRedTekstParagraf").children[0].innerHTML;
	var sporednaSlika = document.getElementById("prviRedDesno2").children[0];
	var sporednaSlikaSrc = document.getElementById("prviRedDesno2").children[0].src;
	var sporednaSlikaAlt = document.getElementById("prviRedDesno2").children[0].alt;
	var sporedniTekst = document.getElementById("prviRedDesno2").children[1].innerHTML;
	glavnaSlika.setAttribute("src", sporednaSlikaSrc);
	sporednaSlika.setAttribute("src", glavnaSlikaSrc);
	glavnaSlika.setAttribute("alt", sporednaSlikaAlt);
	sporednaSlika.setAttribute("alt", glavnaSlikaAlt);
	document.getElementById("prviRedTekstParagraf").children[0].innerHTML = sporedniTekst;
	document.getElementById("prviRedDesno2").children[1].innerHTML = glavniTekst;
	document.getElementById("prviRedTekstNaslovh1").innerHTML = sporednaSlikaAlt;
}

function postaviKaoGlavnuSliku3() {
	var glavnaSlika = document.getElementById("prviRedLevo").children[0];
	var glavnaSlikaSrc = document.getElementById("prviRedLevo").children[0].src;
	var glavnaSlikaAlt = document.getElementById("prviRedLevo").children[0].alt;
	var glavniTekst = document.getElementById("prviRedTekstParagraf").children[0].innerHTML;
	var sporednaSlika = document.getElementById("prviRedDesno3").children[0];
	var sporednaSlikaSrc = document.getElementById("prviRedDesno3").children[0].src;
	var sporednaSlikaAlt = document.getElementById("prviRedDesno3").children[0].alt;
	var sporedniTekst = document.getElementById("prviRedDesno3").children[1].innerHTML;
	glavnaSlika.setAttribute("src", sporednaSlikaSrc);
	sporednaSlika.setAttribute("src", glavnaSlikaSrc);
	glavnaSlika.setAttribute("alt", sporednaSlikaAlt);
	sporednaSlika.setAttribute("alt", glavnaSlikaAlt);
	document.getElementById("prviRedTekstParagraf").children[0].innerHTML = sporedniTekst;
	document.getElementById("prviRedDesno3").children[1].innerHTML = glavniTekst;
	document.getElementById("prviRedTekstNaslovh1").innerHTML = sporednaSlikaAlt;
}

function logout() {
	localStorage.removeItem("aktivniKorisnik");
	window.location.replace("login.html");
}

function PodaciOAktivnomKorisniku() {
	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	var adresa = aktivniKorisnik["slika"];
	document.getElementById("slikaKorisnika").src = adresa;
}

function prikaziFormuZaDodavanjeNovogProdavca() {
	var FormaZaDodavanjeNovogProdavcaDiv = document.getElementById("drugiRedDesno").children[0];
	FormaZaDodavanjeNovogProdavcaDiv.setAttribute("style", "display:block");	
	document.getElementById("korisnickoImeInput").value = "";
	document.getElementById("sifraInput").value = "";
	document.getElementById("imeInput").value = "";
	document.getElementById("prezimeInput").value = "";
	document.getElementById("adresaSlikeNovogProdavcaInput").value = "";
}

function SkloniFormuZaNovogKorisnika() {
	var FormaZaDodavanjeNovogProdavcaDiv = document.getElementById("drugiRedDesno").children[0];
	FormaZaDodavanjeNovogProdavcaDiv.setAttribute("style", "display:none");
}

function dodajNovogProdavcaULocalStorage() {

	var korisnickoImeInput  = document.getElementById("korisnickoImeInput").value;
	var sifraInput  = document.getElementById("sifraInput").value;
	var imeInput  = document.getElementById("imeInput").value;
	var prezimeInput  = document.getElementById("prezimeInput").value;
	var adresaSlikeNovogProdavcaInput  = document.getElementById("adresaSlikeNovogProdavcaInput").value;
	var ulogaInput  = document.getElementById("ulogaInput").value;
	
	var korisnici = localStorage.getItem("korisnici");
	korisnici = JSON.parse(korisnici);
	for (let i = 0; i < korisnici.length; i++) {
		if (korisnickoImeInput != korisnici[i]["korisnicko_ime"] && sifraInput != korisnici[i]["sifra"]) {

			if (korisnickoImeInput != "" && sifraInput != "" && imeInput != "" && prezimeInput != "" && adresaSlikeNovogProdavcaInput != "") {
				
				var divZaFormuNovogProdavca = document.getElementById("formaZaNovogKorisnika");
				divZaFormuNovogProdavca.setAttribute("style", "display:none");
				var divZaPotvrduFormeNovogProdavca = document.getElementById("potvrdaFormeZaNovogKorisnika");
				divZaPotvrduFormeNovogProdavca.setAttribute("style", "display:block");

				document.getElementById("korisnickoImeNovogKorisnikaInput").value = korisnickoImeInput;
				document.getElementById("imeNovogKorisnikaInput").value = imeInput;
				document.getElementById("prezimeNovogKorisnikaInput").value = prezimeInput;
				document.getElementById("slikaNovogProdavcaImg").src = adresaSlikeNovogProdavcaInput;
			} else {
				window.alert("Molimo unesite sve potrebne podatke")
			}
		} else {
			window.alert("Korisničko ime ili šifra su zauzeti")
		}
	return korisnickoImeInput, sifraInput, imeInput, prezimeInput, adresaSlikeNovogProdavcaInput, ulogaInput;
}}

function prekiniDodavanjeNovogProdavca() {
	var divZaPotvrduFormeNovogProdavca = document.getElementById("potvrdaFormeZaNovogKorisnika");
	divZaPotvrduFormeNovogProdavca.setAttribute("style", "display:none");

	document.getElementById("korisnickoImeInput").value = "";
	document.getElementById("sifraInput").value = "";
	document.getElementById("imeInput").value = "";
	document.getElementById("prezimeInput").value = "";
	document.getElementById("adresaSlikeNovogProdavcaInput").value = "";

	var divZaFormuNovogProdavca = document.getElementById("formaZaNovogKorisnika");
	divZaFormuNovogProdavca.setAttribute("style", "display:block");
}

/*function prikaziFormuZaDodavanjeNovogProdavcaPonovo() {
	var divZaPotvrduFormeNovogProdavca = document.getElementById("potvrdaFormeZaNovogKorisnika");
	divZaPotvrduFormeNovogProdavca.setAttribute("style", "display:none");
	var FormaZaDodavanjeNovogProdavcaDiv = document.getElementById("drugiRedDesno").children[0];
	FormaZaDodavanjeNovogProdavcaDiv.setAttribute("style", "display:block");	document.getElementById("korisnickoImeInput").value = "";
	document.getElementById("sifraInput").value = "";
	document.getElementById("imeInput").value = "";
	document.getElementById("prezimeInput").value = "";
	document.getElementById("adresaSlikeNovogProdavcaInput").value = "";
}*/

function dodajNovogProdavcaUSistem() {
	var noviProdavac = {
			"korisnicko_ime": korisnickoImeInput.value,
			"sifra": sifraInput.value,
			"ime": imeInput.value,
			"prezime": prezimeInput.value,
			"slika": adresaSlikeNovogProdavcaInput.value,
			"uloga": ulogaInput.value
		}

	var korisnici = JSON.parse(localStorage.getItem("korisnici"));
	korisnici.push(noviProdavac);
	localStorage.setItem("korisnici", JSON.stringify(korisnici));

	window.alert("Prodavac je uspešno unet u sistem");

	var divZaPotvrduFormeNovogProdavca = document.getElementById("potvrdaFormeZaNovogKorisnika");
	divZaPotvrduFormeNovogProdavca.setAttribute("style", "display:none");

	document.getElementById("korisnickoImeInput").value = "";
	document.getElementById("sifraInput").value = "";
	document.getElementById("imeInput").value = "";
	document.getElementById("prezimeInput").value = "";
	document.getElementById("adresaSlikeNovogProdavcaInput").value = "";

	var divZaFormuNovogProdavca = document.getElementById("formaZaNovogKorisnika");
	divZaFormuNovogProdavca.setAttribute("style", "display:block");
}

/*function proveriAktivnogKorisnika() {
	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	if (aktivniKorisnik["uloga"] == "menadzer") {
		if(isMeniClicked === true){
		var dodavanjeProdavca = document.getElementById("dodavanjeProdavca");
		console.log(dodavanjeProdavca);
	}
	}
}*/

function postaviPodatkeOAktivnomKorisnikuIUcitajLocalStorage() {
	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	var src = aktivniKorisnik["slika"];
	document.getElementById("nultiRedGoreSlikaKorisnikaImg").src = src;

	if (typeof(Storage) !== "undefined") {
		var postojanjeFilmovi = localStorage.getItem(filmovi);
		if (!postojanjeFilmovi) {
			localStorage.setItem("filmovi", JSON.stringify(filmovi));
		}
		var postojanjeZanrovi = localStorage.getItem(zanrovi);
		if (!postojanjeZanrovi) {
			localStorage.setItem("zanrovi", JSON.stringify(zanrovi));
		}
	}
	
	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("projekcije");
		if (!postojanje) {
			localStorage.setItem("projekcije", JSON.stringify(projekcije));
		}
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("sale");
		if (!postojanje) {
			localStorage.setItem("sale", JSON.stringify(sale));
		}
	}
	proveraDatumaProjekcijaISadasnjegDatuma();
}

function proveraDatumaProjekcijaISadasnjegDatuma() {
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	var danasnjiDatum = new Date();
	/*console.log(danasnjiDatum);*/
	
	var istekleProjkecije = false;
	for (let i = 0; i < projekcije.length; i++) {
		/*console.log(projekcije[i]["idProjekcije"]);*/
		var pocetakProjekcije = projekcije[i]["pocetakProjekcije"];
		pocetakProjekcije = pocetakProjekcije.split("|");
		var pocetakProjekcijeGodina = pocetakProjekcije[0];
		var pocetakProjekcijeMesec = pocetakProjekcije[1];
		var pocetakProjekcijeDan = pocetakProjekcije[2];
		var pocetakProjekcijeSati = pocetakProjekcije[3];
		var pocetakProjekcijeMinuti = pocetakProjekcije[4];
		pocetakProjekcije = new Date(pocetakProjekcijeGodina, pocetakProjekcijeMesec, pocetakProjekcijeDan, 
			pocetakProjekcijeSati, pocetakProjekcijeMinuti);
		/*console.log(pocetakProjekcije);*/
		if (pocetakProjekcije <= danasnjiDatum) {
			if (projekcije[i]["obrisana"] == false) {
				projekcije[i]["obrisana"] = true;
				localStorage.setItem("projekcije", JSON.stringify(projekcije));
				istekleProjkecije = true;
			}
		}
	}
	if (istekleProjkecije) {
		window.alert("Jedna ili nekoliko od postojanih projekcija su istekle.");
	}
}

function otvoriStranicuFilmovi() {
	window.location.replace("filmovi.html");
}

function prikaziFormuZaDodavanjeNovogFilma() {
	var formaZaNoviFilm = document.getElementById("formaZaNoviFilm");
	formaZaNoviFilm.setAttribute("style", "display:block");
}

function unesitePodatkeOFilmu() {
	var naziv = document.getElementById("formaZaNoviFilmNaziv").children[0].value;
	var poster = document.getElementById("formaZaNoviFilmPoster").children[0].value;
	var zanr = document.getElementById("formaZaNoviFilmZanr").children[0].value;
	var yturl = document.getElementById("formaZaNoviFilmYturl").children[0].value;
	var trajanje = document.getElementById("formaZaNoviFilmTrajanje").children[0].value;



	var filmovi = localStorage.getItem("filmovi");
	filmovi = JSON.parse(filmovi);
	var idFilma = 1;

	for (let i = 0; i<filmovi.length; i++) {
		idFilma = idFilma + 1;
	}


	var zanrovi = localStorage.getItem("zanrovi");
	zanrovi = JSON.parse(zanrovi);


	var stanjeZanra = "ne postoji";
	for (let j = 0; j<zanrovi.length; j++) {
		if (zanr == zanrovi[j]) {
			stanjeZanra = "postoji";
		}
	}

	if (stanjeZanra == "ne postoji") {
		window.alert("Uneti žanr ne postoji u sistemu, molimo probajte ponovo.")
		return;
	}

	/*console.log(zanr);
	console.log(odobrenjeZanra);*/



	/*var has = /[0-9]/.test('12341');*/
	
	/*console.log("123123asd".isNumber()); // outputs true*/
	/*console.log("+12".isNumber()); // outputs false*/


	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
	var sati = 0;

	if (trajanje.isNumber() == true) {
		window.alert("Za unos trajanja filma koristite brojeve.");
		return;
	}

	trajanje = parseInt(trajanje);

	if (trajanje<20) {
		window.alert("Film ne može da traje manje od 20 minuta.");
		return;
	}
	while (trajanje>=60) {
		sati = sati + 1;
		trajanje = trajanje - 60;
	}

	if (sati>10) {
		window.alert("Film ne može da traje duže od 10 sati.");
		return;
	}

	if (trajanje<10) {
		trajanje = trajanje.toString();
		trajanje = "0" + trajanje;
	}

	if (sati>0) {
		trajanje = trajanje.toString();
		sati = sati.toString();
		trajanje = "0" + sati + trajanje;
	} else {
		trajanje = trajanje.toString();
		trajanje = "00" + trajanje;
	}

	/*console.log(trajanje);*/

	sati = trajanje.slice(0, 2);
	minuti = trajanje.slice(2, 4);
	trajanje = sati + "|" + minuti;

	var noviFilm = {
		"idFilma": idFilma,
        "naziv": naziv,
		"poster": poster,
		"zanr": zanr,
		"yturl": yturl,
		"trajanje": trajanje}

	var filmovi = JSON.parse(localStorage.getItem("filmovi"));
	filmovi.push(noviFilm);
	localStorage.setItem("filmovi", JSON.stringify(filmovi));

	window.alert("Film je uspešno unet u sistem.");	
}

function unosNoveProjekcije() {
	window.location.replace("projekcije.html");
}

function brisanjeIIzmenaProjekcije() {
	window.location.replace("projekcije.html");
}

function prikazPretrage() {
	window.location.replace("projekcije.html");
}

function windowAlertZaKontakt() {
	window.alert("Kontakt telefon: 021/495-652.")
}