var filmovi = [{"idFilma": "1",
            "naziv": "A Bad Mom's Christmas",
			"poster": "../data/imgs/posteri/A Bad Mom's Christmas.jpg",
			"zanr": "comedy",
			"yturl": "https://www.youtube.com/embed/EQPr0sCjau0?ecver=1",
			"trajanje": "01|44"},
			{"idFilma": "2",
            "naziv": "Star Wars - The Last Jedi",
			"poster": "../data/imgs/posteri/Star Wars - The Last Jedi.jpg",
			"zanr": "fantasy",
			"yturl": "https://www.youtube.com/embed/Q0CbN8sfihY?ecver=1",
			"trajanje": "02|32"},
			{"idFilma": "3",
            "naziv": "The Godfather",
			"poster": "../data/imgs/posteri/The Godfather.jpg",
			"zanr": "drama",
			"yturl": "https://www.youtube.com/embed/sY1S34973zA?ecver=1",
			"trajanje": "02|58"},
			{"idFilma": "4",
            "naziv": "Titanic",
			"poster": "../data/imgs/posteri/Titanic.jpg",
			"zanr": "drama",
			"yturl": "https://www.youtube.com/embed/ZQ6klONCq4s?ecver=1",
			"trajanje": "03|14"}]


function logout() {
	localStorage.removeItem("aktivniKorisnik");
	window.location.replace("login.html");
}

function otvoriStranicuPocetna() {
	window.location.replace("main.html");
}

function postaviPodatkeOAktivnomKorisnikuIFilmovima() {
	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("filmovi");
		if (!postojanje) {
			localStorage.setItem("filmovi", JSON.stringify(filmovi));
		}
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	var src = aktivniKorisnik["slika"];
	document.getElementById("nultiRedGoreSlikaKorisnikaImg").src = src;

	var filmovi = localStorage.getItem("filmovi");
	filmovi = JSON.parse(filmovi);
	/*console.log(filmovi[0]);*/

	var visinaDrugiRedLeviIDesniDiv = 100;

	for (let i = 0; i < filmovi.length; i++) {
		var poster = filmovi[i]["poster"];
		var idFilma = filmovi[i]["idFilma"];
		var zanr = filmovi[i]["zanr"];
		var naziv = filmovi[i]["naziv"];
		var yturl = filmovi[i]["yturl"];

		var div = document.getElementById("drugiRedPrikazFilmovaGlavniDiv");
		var divZaFilm = document.createElement("div");
		divZaFilm.setAttribute("style", "width:100%; height: 200px; border-top:1.5px dotted black");
		var divZaFilmNaziv = document.createElement("div");
		divZaFilmNaziv.setAttribute("style", "position:relative; left:20%; bottom:71%; width:16%; height: 75%; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
		var divZaFilmNazivParagraf = document.createElement("p");
		divZaFilmNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
		var tekstDivZaFilmNaziv = document.createTextNode(naziv);
		divZaFilmNazivParagraf.appendChild(tekstDivZaFilmNaziv);
		divZaFilmNaziv.appendChild(divZaFilmNazivParagraf);
		var divZaFilmNazivParagrafZaZanrIspis = document.createElement("p");
		divZaFilmNazivParagrafZaZanrIspis.setAttribute("style", "text-align:center; font-family:trebuchet ms;");
		var tekstDivZaFilmNazivParagrafZaZanrIspis = document.createTextNode("žanr:");
		divZaFilmNazivParagrafZaZanrIspis.appendChild(tekstDivZaFilmNazivParagrafZaZanrIspis);
		divZaFilmNaziv.appendChild(divZaFilmNazivParagrafZaZanrIspis);
		var divZaFilmNazivParagrafZaZanr = document.createElement("p");
		divZaFilmNazivParagrafZaZanr.setAttribute("style", "font-style: italic; text-align:center; font-family:trebuchet ms; font-weight: bold;");
		var tekstDivZaFilmNazivParagrafZaZanr = document.createTextNode(zanr);
		divZaFilmNazivParagrafZaZanr.appendChild(tekstDivZaFilmNazivParagrafZaZanr);
		divZaFilmNaziv.appendChild(divZaFilmNazivParagrafZaZanr);
		var divZaFilmYouTube = document.createElement("div");
		divZaFilmYouTube.setAttribute("style", "position:relative; left:84%; bottom:147%; width:16%; height: 75%;");
		var divZaFilmYouTubeiFrame = document.createElement("iframe");
		divZaFilmYouTubeiFrame.setAttribute("src", yturl);
		divZaFilmYouTubeiFrame.setAttribute("style", "width:100%; height:100%;");
		divZaFilmYouTubeiFrame.setAttribute("allowfullscreen", "allowfullscreen");
		divZaFilmYouTube.appendChild(divZaFilmYouTubeiFrame);
		var divZaFilmPoster = document.createElement("div");
		divZaFilmPoster.setAttribute("style", "position:relative; top:12.5%; width:16%; height: 75%; border:1px solid black;");
		divZaFilmPoster.setAttribute("id", "divZaFilmPoster");
		var divZaFilmPosterImg = document.createElement("img");
		divZaFilmPosterImg.setAttribute("alt", "Pogrešna adresa za poster filma");
		divZaFilmPosterImg.setAttribute("style", "width:100%; height:100%;");
		divZaFilmPosterImg.setAttribute("src", poster);
		divZaFilmPoster.appendChild(divZaFilmPosterImg);
		divZaFilm.appendChild(divZaFilmPoster);
		divZaFilm.appendChild(divZaFilmNaziv);
		divZaFilm.appendChild(divZaFilmYouTube);

		div.appendChild(divZaFilm);

		visinaDrugiRedLeviIDesniDiv = visinaDrugiRedLeviIDesniDiv + 200;
	}

	visinaDrugiRedLeviIDesniDiv = visinaDrugiRedLeviIDesniDiv.toString();

	document.getElementById("drugiRedLeviDiv").style.height = visinaDrugiRedLeviIDesniDiv + "px";
	document.getElementById("drugiRedDesniDiv").style.height = visinaDrugiRedLeviIDesniDiv + "px";
}

function prekoLogoaVratiNaPocetnu() {
	window.location.replace("main.html");
}

function windowAlertZaKontakt() {
	window.alert("Kontakt telefon: 021/495-652.")
}