var racun = []
var privremeniRacun = []

/*var racun = [{"idRacuna": "",
			"datumIVreme": "",
			"ukupnaCena": ""}]
var privremeniRacun = [{"idProjekcije": "",
						"nazivFilma": "",
						"pocetakProjekcije": pocetakProjekcije,
						"brojKarata": "",
						"cenaKarte": "",
						"ukupnaCenaKarata": ""}]*/

function ucitajPotrebnePodatke() {

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("projekcije");
		if (!postojanje) {
			localStorage.setItem("projekcije", JSON.stringify(projekcije));
		}
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("racun");
		if (!postojanje) {
			localStorage.setItem("racun", JSON.stringify(racun));
		}
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("privremeniRacun");
		if (!postojanje) {
			localStorage.setItem("privremeniRacun", JSON.stringify(privremeniRacun));
		}
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("sale");
		if (!postojanje) {
			localStorage.setItem("sale", JSON.stringify(sale));
		}
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	var src = aktivniKorisnik["slika"];
	document.getElementById("nultiRedGoreSlikaKorisnikaImg").src = src;

	var today = new Date();
	var dd = today.getDate(); 
	var mm = today.getMonth()+1;
	//January is 0! 
	var yyyy = today.getFullYear(); 

	/*console.log(today);*/
	if (dd<10) {
		dd = dd.toString();
		dd ='0' + dd;
	}

	if (mm<10) {
		mm = mm.toString();
		mm='0'+mm;
	}
	var today = dd+'/'+mm+'/'+yyyy;

	/*document.getElementById("sadasnjiDatumDatum").innerHTML = today;*/
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));

	for (let i = 0; i < projekcije.length; i++) {
		
		if (projekcije[i]["obrisana"] == false) {

			var nazivFilmaPoIDju = projekcije[i]["nazivFilma"];
			var pocetakProjekcijePoIDju = projekcije[i]["pocetakProjekcije"];
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.split("|");
			var pocetakProjekcijePoIDjuGodina = pocetakProjekcijePoIDju[0];
			var pocetakProjekcijePoIDjuMesec = pocetakProjekcijePoIDju[1];
			var pocetakProjekcijePoIDjuDan = pocetakProjekcijePoIDju[2];
			var pocetakProjekcijePoIDjuSati = pocetakProjekcijePoIDju[3];
			var pocetakProjekcijePoIDjuMinuti = pocetakProjekcijePoIDju[4];
			pocetakProjekcijePoIDju = new Date(pocetakProjekcijePoIDjuGodina, pocetakProjekcijePoIDjuMesec, pocetakProjekcijePoIDjuDan, 
				pocetakProjekcijePoIDjuSati, pocetakProjekcijePoIDjuMinuti);
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.toString();
			var cenaProjekcijePoIDju = projekcije[i]["cena"];
			var idProjekcije = projekcije[i]["idProjekcije"];


			var div = document.getElementById("prviRedProjekcije");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekciju" + idProjekcije);
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilmaPoIDju);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcijePoIDju);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcijePoIDju + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuKupiKartu = document.createElement("div");
			divZaProjekcijuKupiKartu.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			divZaProjekcijuKupiKartu.setAttribute("onclick", "unesiteBrojKarata(" + "divZaProjekciju" + idProjekcije + ")");
			var divZaProjekcijuKupiKartuParagraf = document.createElement("p");
			divZaProjekcijuKupiKartuParagraf.setAttribute("style", "cursor:pointer; text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuKupiKartu = document.createTextNode("Dodaj u korpu");
			divZaProjekcijuKupiKartuParagraf.appendChild(tekstDivZaProjekcijuKupiKartu);
			divZaProjekcijuKupiKartu.appendChild(divZaProjekcijuKupiKartuParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);

			if (aktivniKorisnik["uloga"] == "prodavac") {
				divZaProjekciju.appendChild(divZaProjekcijuKupiKartu);
			}

			div.appendChild(divZaProjekciju);
		}
	}
}

function logout() {
	localStorage.removeItem("aktivniKorisnik");
	window.location.replace("login.html");
}

function prekoLogoaVratiNaPocetnu() {
	window.location.replace("main.html");
}

function otvoriStranicuPocetna() {
	window.location.replace("main.html");
}

var isUnesiteBrojKarataClicked = false;
function unesiteBrojKarata(idDivZaProjekciju) {
	var idRoditelja = idDivZaProjekciju.id;
	var idProjekcije = idRoditelja.replace("divZaProjekciju", "");
	/*console.log(idProjekcije);*/

	if(isUnesiteBrojKarataClicked === false){
		var div = idDivZaProjekciju;
		var divZabrojKarata = document.createElement("div");
		divZabrojKarata.setAttribute("style", "position:relative; width:100%; height: 50px;");
		var divZaBrojKarataTekst = document.createElement("div");
		divZaBrojKarataTekst.setAttribute("style", "position:absolute; left:0; width:30%;");
		var tekstZaBrojKarataParagraf = document.createElement("p");
		tekstZaBrojKarataParagraf.setAttribute("style", "position:absolute; right:0; font-family:trebuchet ms; font-weight: bold;");
		var tekstZaBrojKarata = document.createTextNode("Unesite broj karata: ");
		tekstZaBrojKarataParagraf.appendChild(tekstZaBrojKarata);
		divZaBrojKarataTekst.appendChild(tekstZaBrojKarataParagraf);
		var divZaBrojKarataInput = document.createElement("div");
		divZaBrojKarataInput.setAttribute("style", "position:absolute; right:33%; width:30%;");
		var inputZaBrojKarata = document.createElement("input");
		inputZaBrojKarata.setAttribute("style", "position:absolute; left:0%; top:8px; width:30%; height:30px;");
		inputZaBrojKarata.setAttribute("id", "inputZaBrojKarata");
		divZaBrojKarataInput.appendChild(inputZaBrojKarata);
		var divZaBrojKarataDodaj = document.createElement("div");
		divZaBrojKarataDodaj.setAttribute("style", "position:absolute; right:0; width:30%;");
		var tekstZaBrojKarataDodajParagraf = document.createElement("p");
		tekstZaBrojKarataDodajParagraf.setAttribute("style", "cursor:pointer; position:absolute; left:0; font-family:trebuchet ms; font-weight: bold;");
		tekstZaBrojKarataDodajParagraf.setAttribute("onclick", "dodajNaRacun("+ idRoditelja + ", " + idProjekcije + ")");
		var tekstZaBrojKarataDodaj = document.createTextNode("Dodaj");
		tekstZaBrojKarataDodajParagraf.appendChild(tekstZaBrojKarataDodaj);
		divZaBrojKarataDodaj.appendChild(tekstZaBrojKarataDodajParagraf);

		divZabrojKarata.appendChild(divZaBrojKarataTekst);
		divZabrojKarata.appendChild(divZaBrojKarataInput);
		divZabrojKarata.appendChild(divZaBrojKarataDodaj);
		div.appendChild(divZabrojKarata);
	}
	isUnesiteBrojKarataClicked = true;
	return isUnesiteBrojKarataClicked;
}

function dodajNaRacun(idRoditelja, idProjekcije) {
	var idRoditelja = idRoditelja.id;
	var idProjekcije = idProjekcije.toString();
	/*console.log(idProjekcije);*/

	var inputZaBrojKarata = document.getElementById("inputZaBrojKarata").value;
	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
	var brojKarataJeNula = false;
		
	if (inputZaBrojKarata.isNumber() == true) {
		window.alert("Unesite broj karata.");
		return;
	}
		
	if (inputZaBrojKarata == "") {
		brojKarataJeNula = true;
	}

	inputZaBrojKarata = parseInt(inputZaBrojKarata);
	if (inputZaBrojKarata <= 0) {
		window.alert("Unesite broj karata.");
		return;
	}
	inputZaBrojKarata = inputZaBrojKarata.toString();

	var divRoditelj = document.getElementById(idRoditelja);
	divRoditelj.removeChild(divRoditelj.childNodes[4]);
	isUnesiteBrojKarataClicked = false;

	if (!brojKarataJeNula) {
		var projekcije = JSON.parse(localStorage.getItem("projekcije"));
		for (let i = 0; i < projekcije.length; i++) {
			if (idProjekcije == projekcije[i]["idProjekcije"]) {
				var cenaKarte = projekcije[i]["cena"];
			}
		}

		cenaKarte = parseInt(cenaKarte);
		inputZaBrojKarata = parseInt(inputZaBrojKarata);
		var ukupnaCenaKarata = inputZaBrojKarata * cenaKarte;
		cenaKarte = cenaKarte.toString();
		inputZaBrojKarata = inputZaBrojKarata.toString();
		ukupnaCenaKarata = ukupnaCenaKarata.toString();

		dodajNaRacunPrivremeno(idProjekcije, inputZaBrojKarata, cenaKarte, ukupnaCenaKarata);
	}

	return isUnesiteBrojKarataClicked;
}

function dodajNaRacunPrivremeno(idProjekcije, brojKarata, cenaKarte, ukupnaCenaKarata) {
	var privremeniRacun = JSON.parse(localStorage.getItem("privremeniRacun"));
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	/*console.log(idProjekcije);
	console.log(brojKarata);
	console.log(cenaKarte);
	console.log(ukupnaCenaKarata);
	console.log(typeof idProjekcije);
	console.log(typeof brojKarata);
	console.log(typeof cenaKarte);
	console.log(typeof ukupnaCenaKarata);*/
	var taProjekcijaPostojiUPrivremenomRacunu = false;
	for (let i = 0; i < projekcije.length; i++) {
		if (idProjekcije == projekcije[i]["idProjekcije"]) {
			var nazivFilma = projekcije[i]["nazivFilma"];
			var pocetakProjekcije = projekcije[i]["pocetakProjekcije"];
		}
	}

	var jedanPrivremeniRacun = {
		"idProjekcije": idProjekcije,
		"nazivFilma": nazivFilma,
		"pocetakProjekcije": pocetakProjekcije,
		"brojKarata": brojKarata,
		"cenaKarte": cenaKarte,
		"ukupnaCenaKarata": ukupnaCenaKarata,
	}

	for (let i = 0; i < privremeniRacun.length; i++) {
		if (idProjekcije == privremeniRacun[i]["idProjekcije"]) {
			privremeniRacun[i]["brojKarata"] = parseInt(privremeniRacun[i]["brojKarata"]) + parseInt(brojKarata);
			privremeniRacun[i]["ukupnaCenaKarata"] = privremeniRacun[i]["brojKarata"] * parseInt(cenaKarte);
			privremeniRacun[i]["brojKarata"] = privremeniRacun[i]["brojKarata"].toString();
			privremeniRacun[i]["ukupnaCenaKarata"] = privremeniRacun[i]["ukupnaCenaKarata"].toString();
			taProjekcijaPostojiUPrivremenomRacunu = true;
		}
	}
	if (!taProjekcijaPostojiUPrivremenomRacunu) {
		privremeniRacun.push(jedanPrivremeniRacun);
	}

	localStorage.setItem("privremeniRacun", JSON.stringify(privremeniRacun));
}

var isPrikaziRacunClicked = false;
function prikaziRacun() {
	if(isPrikaziRacunClicked === false){
		var privremeniRacun = JSON.parse(localStorage.getItem("privremeniRacun"));
		var racun = JSON.parse(localStorage.getItem("racun"));

		var ukupnaCena = 0;
		for (let i = 0; i < privremeniRacun.length; i++) {
			ukupnaCena = ukupnaCena + (parseInt(privremeniRacun[i]["brojKarata"]) * parseInt(privremeniRacun[i]["cenaKarte"]));
		}
		ukupnaCena = ukupnaCena.toString();

		var brojac = 0;
		for (let i = 0; i < racun.length; i++) {
			brojac = brojac + 1;
		}
		var idRacuna = brojac + 1;
		idRacuna = idRacuna.toString();

		if (typeof privremeniRacun[0] == "undefined") {
			window.alert("Dodajte nešto u korpu.");
			return;
		}

		for (let i = 0; i < privremeniRacun.length; i++) {
			
			var nazivFilma = privremeniRacun[i]["nazivFilma"];
			var pocetakProjekcije = privremeniRacun[i]["pocetakProjekcije"];
			pocetakProjekcije = pocetakProjekcije.split("|");
			var pocetakProjekcijeGodina = pocetakProjekcije[0];
			var pocetakProjekcijeMesec = pocetakProjekcije[1];
			var pocetakProjekcijeDan = pocetakProjekcije[2];
			var pocetakProjekcijeSati = pocetakProjekcije[3];
			var pocetakProjekcijeMinuti = pocetakProjekcije[4];
			pocetakProjekcije = new Date(pocetakProjekcijeGodina, pocetakProjekcijeMesec, pocetakProjekcijeDan, 
				pocetakProjekcijeSati, pocetakProjekcijeMinuti);
			pocetakProjekcije = pocetakProjekcije.toString();
			var cenaProjekcije = privremeniRacun[i]["cenaKarte"];
			var brojKarata = privremeniRacun[i]["brojKarata"];
			var idProjekcije = privremeniRacun[i]["idProjekcije"];


			var div = document.getElementById("prviRedProjekcijePrikazRacunaPrikaz2");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekcijuRacun");
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilma);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcije);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcije + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuBrojKarata = document.createElement("div");
			divZaProjekcijuBrojKarata.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuBrojKarataParagraf = document.createElement("p");
			divZaProjekcijuBrojKarataParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuBrojKarata = document.createTextNode("Broj karta: " + brojKarata);
			divZaProjekcijuBrojKarataParagraf.appendChild(tekstDivZaProjekcijuBrojKarata);
			divZaProjekcijuBrojKarata.appendChild(divZaProjekcijuBrojKarataParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);
			divZaProjekciju.appendChild(divZaProjekcijuBrojKarata);

			div.appendChild(divZaProjekciju);
		}
		var divZaProjekcijuOpcijeSaRacunom = document.createElement("div");
		divZaProjekcijuOpcijeSaRacunom.setAttribute("style", "border:1px solid black; position:relative; width:100%; height: 100px; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
		var divZaProjekcijuOpcijeSaRacunomParagrafIzvrsiKupovinu = document.createElement("p");
		divZaProjekcijuOpcijeSaRacunomParagrafIzvrsiKupovinu.setAttribute("style", "cursor:pointer; position:absolute; left:2%; top: 30%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
		divZaProjekcijuOpcijeSaRacunomParagrafIzvrsiKupovinu.setAttribute("onclick", "izvrsiKupovinu(" + idRacuna + ", " + ukupnaCena + ")");
		var tekstdivZaProjekcijuOpcijeSaRacunomIzvrsiKupovinu = document.createTextNode("Izvrši kupovinu");
		divZaProjekcijuOpcijeSaRacunomParagrafIzvrsiKupovinu.appendChild(tekstdivZaProjekcijuOpcijeSaRacunomIzvrsiKupovinu);
		divZaProjekcijuOpcijeSaRacunom.appendChild(divZaProjekcijuOpcijeSaRacunomParagrafIzvrsiKupovinu);
		var divZaProjekcijuOpcijeSaRacunomParagrafPonistiRacun = document.createElement("p");
		divZaProjekcijuOpcijeSaRacunomParagrafPonistiRacun.setAttribute("style", "cursor:pointer; position:absolute; right:2%; top: 30%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
		divZaProjekcijuOpcijeSaRacunomParagrafPonistiRacun.setAttribute("onclick", "ponistiRacun()");
		var tekstdivZaProjekcijuOpcijeSaRacunomPonistiRacun = document.createTextNode("Poništi račun");
		divZaProjekcijuOpcijeSaRacunomParagrafPonistiRacun.appendChild(tekstdivZaProjekcijuOpcijeSaRacunomPonistiRacun);
		divZaProjekcijuOpcijeSaRacunom.appendChild(divZaProjekcijuOpcijeSaRacunomParagrafPonistiRacun);
		var divZaProjekcijuOpcijeSaRacunomParagrafUkupnaCena = document.createElement("p");
		divZaProjekcijuOpcijeSaRacunomParagrafUkupnaCena.setAttribute("style", "cursor:default; position:absolute; left:20%; top: 30%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
		var tekstdivZaProjekcijuOpcijeSaRacunomUkupnaCena = document.createTextNode("Ukupno za isplatu: " + ukupnaCena + " dinara");
		divZaProjekcijuOpcijeSaRacunomParagrafUkupnaCena.appendChild(tekstdivZaProjekcijuOpcijeSaRacunomUkupnaCena);
		divZaProjekcijuOpcijeSaRacunom.appendChild(divZaProjekcijuOpcijeSaRacunomParagrafUkupnaCena);
		var divZaProjekcijuOpcijeSaRacunomParagrafDodajJosKarata = document.createElement("p");
		divZaProjekcijuOpcijeSaRacunomParagrafDodajJosKarata.setAttribute("style", "cursor:pointer; position:absolute; left:60%; top: 30%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
		divZaProjekcijuOpcijeSaRacunomParagrafDodajJosKarata.setAttribute("onclick", "dodajJosKarata()");
		var tekstdivZaProjekcijuOpcijeSaRacunomDodajJosKarata = document.createTextNode("Dodaj jos karata");
		divZaProjekcijuOpcijeSaRacunomParagrafDodajJosKarata.appendChild(tekstdivZaProjekcijuOpcijeSaRacunomDodajJosKarata);
		divZaProjekcijuOpcijeSaRacunom.appendChild(divZaProjekcijuOpcijeSaRacunomParagrafDodajJosKarata);

		divZaProjekciju.appendChild(divZaProjekcijuOpcijeSaRacunom);

		div.appendChild(divZaProjekciju);
	}
	isPrikaziRacunClicked = true;
	return isPrikaziRacunClicked;
}

function izvrsiKupovinu(idRacuna, ukupnaCena) {
	var privremeniRacun = JSON.parse(localStorage.getItem("privremeniRacun"));
	var racun = JSON.parse(localStorage.getItem("racun"));
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));

	var datumIVreme = new Date();
	var idRacuna = idRacuna.toString();
	var ukupnaCena = ukupnaCena.toString();

	var nemaTolikoSlobodnihMesta = true;
	for (let i = 0; i < privremeniRacun.length; i++) {
		for (let j = 0; j < projekcije.length; j++) {
			if (privremeniRacun[i]["idProjekcije"] == projekcije[j]["idProjekcije"]) {
				if(parseInt(projekcije[j]["brojSlobodnihMesta"]) >= parseInt(privremeniRacun[i]["brojKarata"])) {
					projekcije[j]["brojSlobodnihMesta"] = parseInt(projekcije[j]["brojSlobodnihMesta"]) - parseInt(privremeniRacun[i]["brojKarata"]);
					projekcije[j]["brojSlobodnihMesta"] = projekcije[j]["brojSlobodnihMesta"].toString();
					nemaTolikoSlobodnihMesta = false;
					localStorage.setItem("projekcije", JSON.stringify(projekcije));
				}
			}
		}
	}
	if (nemaTolikoSlobodnihMesta) {
		window.alert("Nema dovoljan broj slobodnih mesta.");
	}

	if (!nemaTolikoSlobodnihMesta) {
		var jedanRacun = {
			"idRacuna": idRacuna,
			"datumIVreme": datumIVreme,
			"ukupnaCena": ukupnaCena
		}

		racun.push(jedanRacun);
		localStorage.setItem("racun", JSON.stringify(racun));
		window.alert("Kupljeno.");
	}

	privremeniRacun = [];
	localStorage.setItem("privremeniRacun", JSON.stringify(privremeniRacun));

	var prviRedProjekcijePrikazRacunaPrikaz1 = document.getElementById("prviRedProjekcijePrikazRacunaPrikaz1");
	while (prviRedProjekcijePrikazRacunaPrikaz1.firstChild) prviRedProjekcijePrikazRacunaPrikaz1.removeChild(prviRedProjekcijePrikazRacunaPrikaz1.firstChild);
	var prviRedProjekcijePrikazRacunaPrikaz2 = document.createElement("div");
	prviRedProjekcijePrikazRacunaPrikaz2.setAttribute("id", "prviRedProjekcijePrikazRacunaPrikaz2");
	prviRedProjekcijePrikazRacunaPrikaz1.appendChild(prviRedProjekcijePrikazRacunaPrikaz2);

	isPrikaziRacunClicked = false;
	return isPrikaziRacunClicked;
}

function ponistiRacun() {
	window.alert("Račun je poništen.");
	var privremeniRacun = JSON.parse(localStorage.getItem("privremeniRacun"));

	privremeniRacun = [];
	localStorage.setItem("privremeniRacun", JSON.stringify(privremeniRacun));

	var prviRedProjekcijePrikazRacunaPrikaz1 = document.getElementById("prviRedProjekcijePrikazRacunaPrikaz1");
	while (prviRedProjekcijePrikazRacunaPrikaz1.firstChild) prviRedProjekcijePrikazRacunaPrikaz1.removeChild(prviRedProjekcijePrikazRacunaPrikaz1.firstChild);
	var prviRedProjekcijePrikazRacunaPrikaz2 = document.createElement("div");
	prviRedProjekcijePrikazRacunaPrikaz2.setAttribute("id", "prviRedProjekcijePrikazRacunaPrikaz2");
	prviRedProjekcijePrikazRacunaPrikaz1.appendChild(prviRedProjekcijePrikazRacunaPrikaz2);

	isPrikaziRacunClicked = false;
	return isPrikaziRacunClicked;
}

function dodajJosKarata() {
	window.alert("Omogućeno je dodavanje karata.");

	var prviRedProjekcijePrikazRacunaPrikaz1 = document.getElementById("prviRedProjekcijePrikazRacunaPrikaz1");
	while (prviRedProjekcijePrikazRacunaPrikaz1.firstChild) prviRedProjekcijePrikazRacunaPrikaz1.removeChild(prviRedProjekcijePrikazRacunaPrikaz1.firstChild);
	var prviRedProjekcijePrikazRacunaPrikaz2 = document.createElement("div");
	prviRedProjekcijePrikazRacunaPrikaz2.setAttribute("id", "prviRedProjekcijePrikazRacunaPrikaz2");
	prviRedProjekcijePrikazRacunaPrikaz1.appendChild(prviRedProjekcijePrikazRacunaPrikaz2);

	isPrikaziRacunClicked = false;
	return isPrikaziRacunClicked;	
}

function windowAlertZaKontakt() {
	window.alert("Kontakt telefon: 021/495-652.")
}