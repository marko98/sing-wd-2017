var korisnici = [{"korisnicko_ime": "Marko98",
            "sifra": "1",
			"ime": "Marko",
			"prezime": "Zahorodni",
			"slika": "../data/imgs/slikeKorisnika/Marko98resized.jpg",
			"uloga": "menadzer"},
			{"korisnicko_ime": "David98",
            "sifra": "2",
			"ime": "David",
			"prezime": "Zahorodni",
			"slika": "../data/imgs/slikeKorisnika/David98resized.jpg",
			"uloga": "prodavac"}]

function postojanjeKorisnikaULocalStorageu() {
	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("korisnici");
		if (!postojanje) {
			localStorage.setItem("korisnici", JSON.stringify(korisnici));
		}
	}
}

function logovanje() {
	var postoji = false;
	var korisnickoIme = document.getElementById("korisnickoImeInput").value;
	var sifra = document.getElementById("sifraInput").value;
	var korisnici = localStorage.getItem("korisnici");
	korisnici = JSON.parse(korisnici)
	for (let i = 0; i < korisnici.length; i++) {
		if (korisnickoIme == korisnici[i]["korisnicko_ime"] && sifra == korisnici[i]["sifra"]) {
			postoji = true;
			localStorage.setItem("aktivniKorisnik", JSON.stringify(korisnici[i]));
			window.location.replace("main.html");
		}
	}
	if (!postoji) {
		window.alert("Korisnik ne postoji.")
	}
}
