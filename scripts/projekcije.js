var projekcije = [{"idProjekcije": "1",
            "pocetakProjekcije": "2018|03|27|15|45",
            "duzinaProjekcije": "01|44",
        	"cena": "300",
        	"nazivFilma": "A Bad Mom's Christmas",
        	"trailerZaFilm": "https://www.youtube.com/embed/EQPr0sCjau0?ecver=1",
        	"zanrFilma": "comedy",
        	"obrisana": false,
        	"sala": "A1",
        	"brojSlobodnihMesta": "200",
        	"brojMestaUSali": "200"},
			{"idProjekcije": "2",
            "pocetakProjekcije": "2018|05|30|05|20",
            "duzinaProjekcije": "02|32",
        	"cena": "350",
        	"nazivFilma": "Star Wars - The Last Jedi",
        	"trailerZaFilm": "https://www.youtube.com/embed/Q0CbN8sfihY?ecver=1",
        	"zanrFilma": "fantasy",
        	"obrisana": false,
        	"sala": "A2",
        	"brojSlobodnihMesta": "150",
        	"brojMestaUSali": "150"}]

var sale = [{"idSale": "A1",
			"brojMestaUSali": "200"},
			{"idSale": "A2",
			"brojMestaUSali": "150"},
			{"idSale": "B1",
			"brojMestaUSali": "200"},
			{"idSale": "B2",
			"brojMestaUSali": "250"}]

var trazeneProjekcijePoIDju = [{"idProjekcije": ""}]
var trazeneProjekcijePoNazivuFilma = [{"nazivFilma": ""}];
var trazeneProjekcijePoZanruFilma = [{"zanrFilma": ""}];
var trazeneProjekcijePoSaliFilma = [{"sala": ""}];

function logout() {
	localStorage.removeItem("aktivniKorisnik");
	window.location.replace("login.html");
}

function otvoriStranicuPocetna() {
	window.location.replace("main.html");
}

function prekoLogoaVratiNaPocetnu() {
	window.location.replace("main.html");
}

function ucitajVremeIPostaviGranicneVrednosti() {
	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("projekcije");
		if (!postojanje) {
			localStorage.setItem("projekcije", JSON.stringify(projekcije));
		}
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("trazeneProjekcijePoIDju");
		if (!postojanje) {
			localStorage.setItem("trazeneProjekcijePoIDju", JSON.stringify(trazeneProjekcijePoIDju));
		}	
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("trazeneProjekcijePoNazivuFilma");
		if (!postojanje) {
			localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));
		}	
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("trazeneProjekcijePoZanruFilma");
		if (!postojanje) {
			localStorage.setItem("trazeneProjekcijePoZanruFilma", JSON.stringify(trazeneProjekcijePoZanruFilma));
		}	
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("trazeneProjekcijePoSaliFilma");
		if (!postojanje) {
			localStorage.setItem("trazeneProjekcijePoSaliFilma", JSON.stringify(trazeneProjekcijePoSaliFilma));
		}	
	}

	if (typeof(Storage) !== "undefined") {
		var postojanje = localStorage.getItem("sale");
		if (!postojanje) {
			localStorage.setItem("sale", JSON.stringify(sale));
		}
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
	var src = aktivniKorisnik["slika"];
	document.getElementById("nultiRedGoreSlikaKorisnikaImg").src = src;

	var today = new Date();
	var dd = today.getDate(); 
	var mm = today.getMonth()+1;
	//January is 0! 
	var yyyy = today.getFullYear(); 

	/*console.log(today);*/
	if (dd<10) {
		dd = dd.toString();
		dd ='0' + dd;
	}

	if (mm<10) {
		mm = mm.toString();
		mm='0'+mm;
	}
	var today = dd+'/'+mm+'/'+yyyy;

	document.getElementById("sadasnjiDatumDatum").innerHTML = today;
	document.getElementById("formaZaDatumNoveProjekcijeGodinaInput").min = yyyy;
	document.getElementById("formaZaDatumNoveProjekcijeMesecInput").min = 1;
	document.getElementById("formaZaDatumNoveProjekcijeMesecInput").max = 12;
	document.getElementById("formaZaDatumNoveProjekcijeDanInput").min = 1;
	document.getElementById("formaZaDatumNoveProjekcijeDanInput").max = 31;
	document.getElementById("formaZaDatumNoveProjekcijeSatiInput").min = 0;
	document.getElementById("formaZaDatumNoveProjekcijeSatiInput").max = 23;
	document.getElementById("formaZaDatumNoveProjekcijeMinutiInput").min = 0;
	document.getElementById("formaZaDatumNoveProjekcijeMinutiInput").max = 59;

	var filmovi = JSON.parse(localStorage.getItem("filmovi"));
	var selectZaOdabirFilmova = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect");
	var brojFilmova = 0;
	for (let i = 0; i < filmovi.length; i++) {
		var imeFilma = filmovi[i]["naziv"];
		brojFilmova = brojFilmova + 1;

		var optionZaFilmove = document.createElement("option");
		var tekstOptionZaFilmove = document.createTextNode(imeFilma);
		optionZaFilmove.setAttribute("value", imeFilma);
		optionZaFilmove.appendChild(tekstOptionZaFilmove);
		selectZaOdabirFilmova.appendChild(optionZaFilmove);
	}
}

function unesitePodatkeONovojProjekcijiDisplay() {

	isprazniStorageeZaPretraguProjekcija();

	document.getElementById("drugiRed").style.display = "block";
	document.getElementById("treciRed").style.display = "none";

	document.getElementById("petiRedP3").style.cursor = "default";
	document.getElementById("izmenitePodatkeODatumuNoveProjekcije").style.display = "none";
	document.getElementById("unesitePodatkeODatumuNoveProjekcije").style.display = "block";
	document.getElementById("petiRedP4").style.display = "block";	
	document.getElementById("petiRedP4").style.cursor = "pointer";

	document.getElementById("petiRedDiv1").style.display = "none";
	document.getElementById("petiRedDiv2").style.display = "none";
}

function unesitePodatkeONovojProjekciji() {
	var projekcije = localStorage.getItem("projekcije");
	projekcije = JSON.parse(projekcije);
	var filmovi = JSON.parse(localStorage.getItem("filmovi"));
	/*console.log(filmovi);*/

	/*var selectZaOdabirFilmova = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect");
	var brojFilmova = 0;
	for (let i = 0; i < filmovi.length; i++) {
		console.log(filmovi[i]);
		var imeFilma = filmovi[i]["naziv"];
		brojFilmova = brojFilmova + 1;

		var optionZaFilmove = document.createElement("option");
		var tekstOptionZaFilmove = document.createTextNode(imeFilma);
		optionZaFilmove.appendChild(tekstOptionZaFilmove);
		selectZaOdabirFilmova.appendChild(optionZaFilmove);
	}
	console.log(brojFilmova);*/
	var brojFilmova = 0;
	for (let i = 0; i < filmovi.length; i++) {
		brojFilmova = brojFilmova + 1;
	}

	for (let i = 0; i < brojFilmova; i++) {

		var selektovanFilm = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect").children[i].selected;
		if (selektovanFilm == true) {
			var nazivFilma = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect").children[i].value;
		}
	}

	for (let i = 0; i < filmovi.length; i++) {
		if (nazivFilma == filmovi[i]["naziv"]) {
			var trailerZaFilm = filmovi[i]["yturl"];
			var trajanjeOdabranogFilma = filmovi[i]["trajanje"];
		}
	}

	trajanjeOdabranogFilma = trajanjeOdabranogFilma.split("|");
	satiTrajanjaOdabranogFilma = trajanjeOdabranogFilma[0];
	satiTrajanjaOdabranogFilma = parseInt(satiTrajanjaOdabranogFilma);
	satiTrajanjaOdabranogFilma = satiTrajanjaOdabranogFilma * 60;
	/*console.log(satiTrajanjaOdabranogFilma);*/
	minutiTrajanjaOdabranogFilma = trajanjeOdabranogFilma[1];
	minutiTrajanjaOdabranogFilma = parseInt(minutiTrajanjaOdabranogFilma);
	trajanjeOdabranogFilma = satiTrajanjaOdabranogFilma + minutiTrajanjaOdabranogFilma;


	var idProjekcije = 1;
	for (let i = 0; i<projekcije.length; i++) {
		idProjekcije = idProjekcije + 1;
	}

	idProjekcije = idProjekcije.toString();

	var godina = document.getElementById("formaZaDatumNoveProjekcijeGodinaInput").value;
	var mesec = document.getElementById("formaZaDatumNoveProjekcijeMesecInput").value;
	var dan = document.getElementById("formaZaDatumNoveProjekcijeDanInput").value;
	var sati = document.getElementById("formaZaDatumNoveProjekcijeSatiInput").value;
	var minuti = document.getElementById("formaZaDatumNoveProjekcijeMinutiInput").value;

	yyyy = document.getElementById("formaZaDatumNoveProjekcijeGodinaInput").min;
	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
	String.prototype.isLetter = function(){return /[^abc]/.test(this);}

	if (godina.length>4) {
		window.alert("Unesite tekuću ili neku od narednih godina, poslednja moguća godina za unos je 9999");
		return;
	}

	if (godina<yyyy) {
		window.alert("Unesite tekuću ili neku od narednih godina");
		return;
	}


	if (mesec.isLetter() == false) {
		window.alert("Unesite jedan od postojećih meseca brojevima");
		return;
	}

	if (mesec<=0 || mesec>12) {
		window.alert("Unesite jedan od postojećih meseca");
		return;
	}


	if (dan.isLetter() == false) {
		window.alert("Unesite jedan od postojećih dana brojevima");
		return;
	}

	if (dan<=0 || dan>31) {
		window.alert("Unesite jedan od postojećih dana u mesecu");
		return;
	}


	if (mesec == 2) {
		if (dan>28) {
		window.alert("Mesec Februar ima 28 dana");
		return;
		}	
	}

	if (mesec == 4 || mesec == 6 || mesec == 9 || mesec == 11) {
		if (dan>30) {
		window.alert("Izabrani mesec ima 30 dana");
		return;
		}	
	}

	if (mesec<10 && mesec.length == 1) {
		mesec = mesec.toString();
		mesec='0'+mesec;
	} 

	if (mesec.length>2) {
		window.alert("Unesite jedan od postojećih meseca");
		return;
	}	


	if (dan<10 && dan.length == 1) {
		dan = dan.toString();
		dan ='0' + dan;
	}

	if (dan.length>2) {
		window.alert("Unesite jedan od postojećih dana u mesecu");
		return;
	}


	if (sati.isLetter() == false) {
		window.alert("Unesite sate");
		return;
	}

	if (sati<0 || sati>23) {
		window.alert("Unesite sate");
		return;
	}

	if (sati<10 && sati.length == 1) {
		sati = sati.toString();
		sati ='0' + sati;
	}

	if (sati.length>2) {
		window.alert("Unesite sate");
		return;
	}


	if (minuti.isLetter() == false) {
		window.alert("Unesite minute");
		return;
	}

	if (minuti<0 || minuti>59) {
		window.alert("Unesite minute");
		return;
	}

	if (minuti<10 && minuti.length == 1) {
		minuti = minuti.toString();
		minuti ='0' + minuti;
	}

	if (minuti.length>2) {
		window.alert("Unesite minute");
		return;
	}


	var pocetakProjekcije = godina + "|" + mesec + "|" + dan + "|" + sati + "|" + minuti;


	var today = document.getElementById("sadasnjiDatumDatum").innerHTML;
	today = today.split("/");
	trenutniDan = today[0];
	trenutniMesec = today[1];
	trenutnaGodina = today[2];

	if (godina == trenutnaGodina && mesec == trenutniMesec && dan == trenutniDan) {
		window.alert("Nije moguće da današnji datum bude odabran za početak prikazivanja nove projekcije.")
		return;
	}

	// Za trajanje projekcije

	var trajanje = document.getElementById("formaZaTrajanjeNoveProjekcijeInput").value;
	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
	var satiTrajanje = 0;

	if (trajanje.isNumber() == true) {
		window.alert("Za unos trajanja filma koristite brojeve.");
		return;
	}

	if (trajanje == "") {
		window.alert("Unesite trajanje projekcije.");
		return;
	}

	trajanje = parseInt(trajanje);

	if (trajanje<trajanjeOdabranogFilma) {
		window.alert("Trajanje projekcija ne može da bude manje od trajanja samog filma.");
		return;
	}
	while (trajanje>=60) {
		satiTrajanje = satiTrajanje + 1;
		trajanje = trajanje - 60;
	}

	if (satiTrajanje>10) {
		window.alert("Film ne može da traje duže od 10 sati.");
		return;
	}

	if (trajanje<10) {
		trajanje = trajanje.toString();
		trajanje = "0" + trajanje;
	}

	if (satiTrajanje>0) {
		trajanje = trajanje.toString();
		satiTrajanje = satiTrajanje.toString();
		trajanje = "0" + satiTrajanje + trajanje;
	} else {
		trajanje = trajanje.toString();
		trajanje = "00" + trajanje;
	}

	satiTrajanje = trajanje.slice(0, 2);
	minutiTrajanje = trajanje.slice(2, 4);
	trajanje = satiTrajanje + "|" + minutiTrajanje;

	var A1 = document.getElementById("formaZaSaluNoveProjekcijeA1").checked;
	var A2 = document.getElementById("formaZaSaluNoveProjekcijeA2").checked;
	var B1 = document.getElementById("formaZaSaluNoveProjekcijeB1").checked;
	var B2 = document.getElementById("formaZaSaluNoveProjekcijeB2").checked;

	var sala = "A1";
	if (A2 == true) {
		sala = "A2";
	} if (B1 == true) {
		sala = "B1";
	} if (B2 == true) {
		sala = "B2";
	}
	/*console.log(A1);*/

	godina = parseInt(godina);
	mesec = parseInt(mesec);
	mesec = mesec - 1;
	mesec = mesec.toString();
	dan = parseInt(dan);
	sati = parseInt(sati);
	minuti = parseInt(minuti);
	satiTrajanje = parseInt(satiTrajanje);
	minutiTrajanje = parseInt(minutiTrajanje);

	var datumPocetkaProjekcije = new Date(godina, mesec, dan, sati, minuti);
	datumPocetkaProjekcije = datumPocetkaProjekcije.toString();
	var datumKrajaProjekcije = new Date(godina, mesec, dan, sati + satiTrajanje, minuti + minutiTrajanje);
	datumKrajaProjekcije = datumKrajaProjekcije.toString();
	/*console.log(datumPocetkaProjekcije);
	console.log(datumKrajaProjekcije);*/
	var sadasnjiDatum = new Date();
	sadasnjiDatum = sadasnjiDatum.toString();
	/*console.log(sadasnjiDatum);*/

	if (new Date (datumPocetkaProjekcije) > new Date (sadasnjiDatum) == false) {
		window.alert("Nije moguće da uneti datum bude odabran za početak prikazivanja nove projekcije.");
		return;
	}

	// Preklapanje datuma projekcija!
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	for (let i = 0; i < projekcije.length; i++) {
		if (sala == projekcije[i]["sala"]) {
			var datumPostojeceProjekcije = projekcije[i]["pocetakProjekcije"].split("|");
			/*console.log(datumPostojeceProjekcije);*/
			var godinaPostojeceProjekcije = parseInt(datumPostojeceProjekcije[0]);
			var mesecPostojeceProjekcije = parseInt(datumPostojeceProjekcije[1]);
			var danPostojeceProjekcije = parseInt(datumPostojeceProjekcije[2]);
			var satiPostojeceProjekcije = parseInt(datumPostojeceProjekcije[3]);
			var minutiPostojeceProjekcije = parseInt(datumPostojeceProjekcije[4]);
			datumPostojeceProjekcije = new Date (godinaPostojeceProjekcije, mesecPostojeceProjekcije, 
				danPostojeceProjekcije, satiPostojeceProjekcije, minutiPostojeceProjekcije);
			var trajanjePostojeceProjekcije = projekcije[i]["duzinaProjekcije"].split("|");
			var satiTrajanjaPostojeceProjekcije = parseInt(trajanjePostojeceProjekcije[0]);
			var minutiTrajanjaPostojeceProjekcije = parseInt(trajanjePostojeceProjekcije[1]);
			var krajPostojeceProjekcije = new Date(godinaPostojeceProjekcije, mesecPostojeceProjekcije, 
				danPostojeceProjekcije, satiPostojeceProjekcije + satiTrajanjaPostojeceProjekcije, 
				minutiPostojeceProjekcije + minutiTrajanjaPostojeceProjekcije);

			datumPostojeceProjekcije = datumPostojeceProjekcije.toString();
			krajPostojeceProjekcije = krajPostojeceProjekcije.toString();

			if (datumPocetkaProjekcije < datumPostojeceProjekcije && datumKrajaProjekcije > datumPostojeceProjekcije) {
				window.alert("Trenutno postoji projekcija koja se emituje tog datuma u unetom intervalu.");
				return;
			} 

			if (datumPocetkaProjekcije < krajPostojeceProjekcije && datumKrajaProjekcije > krajPostojeceProjekcije) {
				window.alert("Trenutno postoji projekcija koja se emituje tog datuma u unetom intervalu.");
				return;
			}

			/*console.log(datumPostojeceProjekcije);
			console.log(krajPostojeceProjekcije);*/
		}

	}
	

	var cena = document.getElementById("formaZaCenuNoveProjekcijeInput").value;
	/*console.log(cena);*/
	if (cena.isNumber() == true) {
		window.alert("Za unos cene projekcije koristite brojeve.");
		return;
	}

	if (cena == "") {
		window.alert("Unesite cenu projekcije.");
		return;
	}

	cena = parseInt(cena);

	if (cena < 100) {
		window.alert("Najniža cena projekcije je 100 dinara.");
		return;
	}

	cena = cena.toString();


	var sale = JSON.parse(localStorage.getItem("sale"));
	for (let i = 0; i < sale.length; i++) {
		if (sala == sale[i]["idSale"]) {
			var brojMestaUSali = sale[i]["brojMestaUSali"];
		}
	}

	for (let i = 0; i < filmovi.length; i++) {
		if (filmovi[i]["naziv"] == nazivFilma) {
			var zanrFilma = filmovi[i]["zanr"];
		}
	}

	var novaProjekcija = {
			"idProjekcije": idProjekcije,
			"pocetakProjekcije": pocetakProjekcije,
			"duzinaProjekcije": trajanje,
			"cena": cena,
			"nazivFilma": nazivFilma,
			"trailerZaFilm": trailerZaFilm,
			"zanrFilma": zanrFilma,
			"obrisana": false,
			"sala": sala,
			"brojSlobodnihMesta": brojMestaUSali,
			"brojMestaUSali": brojMestaUSali
		}

	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	projekcije.push(novaProjekcija);
	localStorage.setItem("projekcije", JSON.stringify(projekcije));

	window.alert("Nova projekcija je uspešno dodata.");
	location.reload("projekcije.html"); 
}

var isPrikaziProjekcijeClicked = false;
function prikaziProjekcije() {

	isprazniStorageeZaPretraguProjekcija()

	document.getElementById("drugiRed").style.display = "none";
	document.getElementById("treciRed").style.display = "block";

	document.getElementById("petiRedP3").style.cursor = "pointer";
	document.getElementById("petiRedP4").style.cursor = "default";

	document.getElementById("petiRedDiv1").style.display = "none";
	document.getElementById("petiRedDiv2").style.display = "none";

	var projekcije = localStorage.getItem("projekcije");
	projekcije = JSON.parse(projekcije);
	/*console.log(projekcije[0]);*/
	if(isPrikaziProjekcijeClicked === false) {
		for (let i = 0; i < projekcije.length; i++) {
			var idProjekcije = projekcije[i]["idProjekcije"];
			var pocetakProjekcije = projekcije[i]["pocetakProjekcije"];
			pocetakProjekcije = pocetakProjekcije.split("|");
			var pocetakProjekcijeGodina = pocetakProjekcije[0];
			var pocetakProjekcijeMesec = pocetakProjekcije[1];
			var pocetakProjekcijeDan = pocetakProjekcije[2];
			var pocetakProjekcijeSati = pocetakProjekcije[3];
			var pocetakProjekcijeMinuti = pocetakProjekcije[4];
			pocetakProjekcije = new Date(pocetakProjekcijeGodina, pocetakProjekcijeMesec, pocetakProjekcijeDan, 
				pocetakProjekcijeSati, pocetakProjekcijeMinuti);
			pocetakProjekcije = pocetakProjekcije.toString();
			var duzinaProjekcije = projekcije[i]["duzinaProjekcije"];
			var cena = projekcije[i]["cena"];
			var nazivFilma = projekcije[i]["nazivFilma"];
			var trailerZaFilm = projekcije[i]["trailerZaFilm"];
			var obrisana = projekcije[i]["obrisana"];
			var sala = projekcije[i]["sala"];
			var brojSlobodnihMesta = projekcije[i]["brojSlobodnihMesta"];
			var brojMestaUSali = projekcije[i]["brojMestaUSali"];

			
			if (projekcije[i]["obrisana"] == false) {
				var div = document.getElementById("treciRed");
				var divZaProjekciju = document.createElement("div");
				divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
				divZaProjekciju.setAttribute("id", "divZaProjekciju");
				var divZaProjekcijuNaziv = document.createElement("div");
				divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
				var divZaProjekcijuNazivParagraf = document.createElement("p");
				divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
				var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilma);
				divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
				divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
				var divZaProjekcijuDatum = document.createElement("div");
				divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
				var divZaProjekcijuDatumParagraf = document.createElement("p");
				divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
				var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcije);
				divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
				divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
				var divZaProjekcijuCena = document.createElement("div");
				divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
				var divZaProjekcijuCenaParagraf = document.createElement("p");
				divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
				var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cena + " dinara");
				divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
				divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);

				var divZaProjekcijuOpcije = document.createElement("div");
				divZaProjekcijuOpcije.setAttribute("style", "position:relative; width:100%; height: 100px; border-radius: 10px;");
				var divZaProjekcijuOpcijeIzmenaDiv = document.createElement("div");
				divZaProjekcijuOpcijeIzmenaDiv.setAttribute("style", "cursor:pointer; border: 1px solid black; position:absolute; left:0; width:49%; height: auto; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
				divZaProjekcijuOpcijeIzmenaDiv.setAttribute("onclick", "izmeniProjekciju(" + idProjekcije + ")");
				var divZaProjekcijuOpcijeIzmenaParagraf = document.createElement("p");
				divZaProjekcijuOpcijeIzmenaParagraf.setAttribute("style", "width:100%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
				var tekstDivZaProjekcijuOpcijeIzmena = document.createTextNode("Izmenite projekciju");
				divZaProjekcijuOpcijeIzmenaParagraf.appendChild(tekstDivZaProjekcijuOpcijeIzmena);
				divZaProjekcijuOpcijeIzmenaDiv.appendChild(divZaProjekcijuOpcijeIzmenaParagraf);
				divZaProjekcijuOpcije.appendChild(divZaProjekcijuOpcijeIzmenaDiv);
				divZaProjekcijuOpcijeBrisanjeDiv = document.createElement("div");
				divZaProjekcijuOpcijeBrisanjeDiv.setAttribute("style", "cursor:pointer; border: 1px solid black; position:absolute; right:0; width:49%; height: auto; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
				divZaProjekcijuOpcijeBrisanjeDiv.setAttribute("onclick", "brisanjeProjekcije(" + idProjekcije + ")");
				var divZaProjekcijuOpcijeBrisanjeParagraf = document.createElement("p");
				divZaProjekcijuOpcijeBrisanjeParagraf.setAttribute("style", "width:100%; text-align:center; font-family:trebuchet ms; font-weight: bold;");
				var tekstDivZaProjekcijuOpcijeBrisanje = document.createTextNode("Obrišite projekciju");
				divZaProjekcijuOpcijeBrisanjeParagraf.appendChild(tekstDivZaProjekcijuOpcijeBrisanje);
				divZaProjekcijuOpcijeBrisanjeDiv.appendChild(divZaProjekcijuOpcijeBrisanjeParagraf);
				divZaProjekcijuOpcije.appendChild(divZaProjekcijuOpcijeBrisanjeDiv);


				divZaProjekciju.appendChild(divZaProjekcijuNaziv);
				divZaProjekciju.appendChild(divZaProjekcijuDatum);
				divZaProjekciju.appendChild(divZaProjekcijuCena);
				divZaProjekciju.appendChild(divZaProjekcijuOpcije);
				div.appendChild(divZaProjekciju);
			}
		}
	}
	isPrikaziProjekcijeClicked = true;
	return isPrikaziProjekcijeClicked;
}

function izmeniProjekciju(idIzabraneProjekcijeZaIzmenu) {
	document.getElementById("drugiRed").style.display = "block";
	document.getElementById("treciRed").style.display = "none";

	document.getElementById("petiRedP3").style.cursor = "pointer";
	document.getElementById("petiRedP4").style.cursor = "default";
	document.getElementById("izmenitePodatkeODatumuNoveProjekcije").style.display = "block";
	document.getElementById("unesitePodatkeODatumuNoveProjekcije").style.display = "none";

	document.getElementById("petiRedDiv1").style.display = "none";
	document.getElementById("petiRedDiv2").style.display = "none";

	var dugmeZaIzmenuProjekcije = document.getElementById("izmenitePodatkeODatumuNoveProjekcije");
	dugmeZaIzmenuProjekcije.setAttribute("onclick", "izmenitePodatkeOProjekciji(" + idIzabraneProjekcijeZaIzmenu + ")");

}

function izmenitePodatkeOProjekciji(idIzabraneProjekcijeZaIzmenu) {

	var projekcije = localStorage.getItem("projekcije");
	projekcije = JSON.parse(projekcije);
	var filmovi = JSON.parse(localStorage.getItem("filmovi"));
	/*console.log(filmovi);*/

	/*var selectZaOdabirFilmova = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect");
	var brojFilmova = 0;
	for (let i = 0; i < filmovi.length; i++) {
		console.log(filmovi[i]);
		var imeFilma = filmovi[i]["naziv"];
		brojFilmova = brojFilmova + 1;

		var optionZaFilmove = document.createElement("option");
		var tekstOptionZaFilmove = document.createTextNode(imeFilma);
		optionZaFilmove.appendChild(tekstOptionZaFilmove);
		selectZaOdabirFilmova.appendChild(optionZaFilmove);
	}
	console.log(brojFilmova);*/
	for (let i = 0; i < projekcije.length; i ++) {
		if (projekcije[i]["idProjekcije"] == idIzabraneProjekcijeZaIzmenu) {
		
			var brojFilmova = 0;
			for (let i = 0; i < filmovi.length; i++) {
				brojFilmova = brojFilmova + 1;
			}

			for (let i = 0; i < brojFilmova; i++) {

				var selektovanFilm = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect").children[i].selected;
				if (selektovanFilm == true) {
					var nazivFilma = document.getElementById("formaZaIzborFilmaNoveProjekcijeSelect").children[i].value;
				}
			}

			for (let i = 0; i < filmovi.length; i++) {
				if (nazivFilma == filmovi[i]["naziv"]) {
					var trailerZaFilm = filmovi[i]["yturl"];
					var trajanjeOdabranogFilma = filmovi[i]["trajanje"];
				}
			}

			trajanjeOdabranogFilma = trajanjeOdabranogFilma.split("|");
			satiTrajanjaOdabranogFilma = trajanjeOdabranogFilma[0];
			satiTrajanjaOdabranogFilma = parseInt(satiTrajanjaOdabranogFilma);
			satiTrajanjaOdabranogFilma = satiTrajanjaOdabranogFilma * 60;
			/*console.log(satiTrajanjaOdabranogFilma);*/
			minutiTrajanjaOdabranogFilma = trajanjeOdabranogFilma[1];
			minutiTrajanjaOdabranogFilma = parseInt(minutiTrajanjaOdabranogFilma);
			trajanjeOdabranogFilma = satiTrajanjaOdabranogFilma + minutiTrajanjaOdabranogFilma;


			/*var idProjekcije = 1;
			for (let i = 0; i<projekcije.length; i++) {
				idProjekcije = idProjekcije + 1;
			}

			idProjekcije = idProjekcije.toString();*/


			var godina = document.getElementById("formaZaDatumNoveProjekcijeGodinaInput").value;
			var mesec = document.getElementById("formaZaDatumNoveProjekcijeMesecInput").value;
			var dan = document.getElementById("formaZaDatumNoveProjekcijeDanInput").value;
			var sati = document.getElementById("formaZaDatumNoveProjekcijeSatiInput").value;
			var minuti = document.getElementById("formaZaDatumNoveProjekcijeMinutiInput").value;

			yyyy = document.getElementById("formaZaDatumNoveProjekcijeGodinaInput").min;
			String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
			String.prototype.isLetter = function(){return /[^abc]/.test(this);}

			if (godina.length>4) {
				window.alert("Unesite tekuću ili neku od narednih godina, poslednja moguća godina za unos je 9999");
				return;
			}

			if (godina<yyyy) {
				window.alert("Unesite tekuću ili neku od narednih godina");
				return;
			}


			if (mesec.isLetter() == false) {
				window.alert("Unesite jedan od postojećih meseca brojevima");
				return;
			}

			if (mesec<=0 || mesec>12) {
				window.alert("Unesite jedan od postojećih meseca");
				return;
			}


			if (dan.isLetter() == false) {
				window.alert("Unesite jedan od postojećih dana brojevima");
				return;
			}

			if (dan<=0 || dan>31) {
				window.alert("Unesite jedan od postojećih dana u mesecu");
				return;
			}


			if (mesec == 2) {
				if (dan>28) {
				window.alert("Mesec Februar ima 28 dana");
				return;
				}	
			}

			if (mesec == 4 || mesec == 6 || mesec == 9 || mesec == 11) {
				if (dan>30) {
				window.alert("Izabrani mesec ima 30 dana");
				return;
				}	
			}

			if (mesec<10 && mesec.length == 1) {
				mesec = mesec.toString();
				mesec='0'+mesec;
			} 

			if (mesec.length>2) {
				window.alert("Unesite jedan od postojećih meseca");
				return;
			}	


			if (dan<10 && dan.length == 1) {
				dan = dan.toString();
				dan ='0' + dan;
			}

			if (dan.length>2) {
				window.alert("Unesite jedan od postojećih dana u mesecu");
				return;
			}


			if (sati.isLetter() == false) {
				window.alert("Unesite sate");
				return;
			}

			if (sati<0 || sati>23) {
				window.alert("Unesite sate");
				return;
			}

			if (sati<10 && sati.length == 1) {
				sati = sati.toString();
				sati ='0' + sati;
			}

			if (sati.length>2) {
				window.alert("Unesite sate");
				return;
			}


			if (minuti.isLetter() == false) {
				window.alert("Unesite minute");
				return;
			}

			if (minuti<0 || minuti>59) {
				window.alert("Unesite minute");
				return;
			}

			if (minuti<10 && minuti.length == 1) {
				minuti = minuti.toString();
				minuti ='0' + minuti;
			}

			if (minuti.length>2) {
				window.alert("Unesite minute");
				return;
			}


			var pocetakProjekcije = godina + "|" + mesec + "|" + dan + "|" + sati + "|" + minuti;


			var today = document.getElementById("sadasnjiDatumDatum").innerHTML;
			today = today.split("/");
			trenutniDan = today[0];
			trenutniMesec = today[1];
			trenutnaGodina = today[2];

			if (godina == trenutnaGodina && mesec == trenutniMesec && dan == trenutniDan) {
				window.alert("Nije moguće da današnji datum bude odabran za početak prikazivanja nove projekcije.")
				return;
			}

			// Za trajanje projekcije

			var trajanje = document.getElementById("formaZaTrajanjeNoveProjekcijeInput").value;
			String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
			var satiTrajanje = 0;

			if (trajanje.isNumber() == true) {
				window.alert("Za unos trajanja filma koristite brojeve.");
				return;
			}

			if (trajanje == "") {
				window.alert("Unesite trajanje projekcije.");
				return;
			}

			trajanje = parseInt(trajanje);

			if (trajanje<trajanjeOdabranogFilma) {
				window.alert("Trajanje projekcija ne može da bude manje od trajanja samog filma.");
				return;
			}
			while (trajanje>=60) {
				satiTrajanje = satiTrajanje + 1;
				trajanje = trajanje - 60;
			}

			if (satiTrajanje>10) {
				window.alert("Film ne može da traje duže od 10 sati.");
				return;
			}

			if (trajanje<10) {
				trajanje = trajanje.toString();
				trajanje = "0" + trajanje;
			}

			if (satiTrajanje>0) {
				trajanje = trajanje.toString();
				satiTrajanje = satiTrajanje.toString();
				trajanje = "0" + satiTrajanje + trajanje;
			} else {
				trajanje = trajanje.toString();
				trajanje = "00" + trajanje;
			}

			satiTrajanje = trajanje.slice(0, 2);
			minutiTrajanje = trajanje.slice(2, 4);
			trajanje = satiTrajanje + "|" + minutiTrajanje;

			var A1 = document.getElementById("formaZaSaluNoveProjekcijeA1").checked;
			var A2 = document.getElementById("formaZaSaluNoveProjekcijeA2").checked;
			var B1 = document.getElementById("formaZaSaluNoveProjekcijeB1").checked;
			var B2 = document.getElementById("formaZaSaluNoveProjekcijeB2").checked;

			var sala = "A1";
			if (A2 == true) {
				sala = "A2";
			} if (B1 == true) {
				sala = "B1";
			} if (B2 == true) {
				sala = "B2";
			}
			/*console.log(A1);*/

			godina = parseInt(godina);
			mesec = parseInt(mesec);
			mesec = mesec - 1;
			mesec = mesec.toString();
			dan = parseInt(dan);
			sati = parseInt(sati);
			minuti = parseInt(minuti);
			satiTrajanje = parseInt(satiTrajanje);
			minutiTrajanje = parseInt(minutiTrajanje);

			var datumPocetkaProjekcije = new Date(godina, mesec, dan, sati, minuti);
			datumPocetkaProjekcije = datumPocetkaProjekcije.toString();
			var datumKrajaProjekcije = new Date(godina, mesec, dan, sati + satiTrajanje, minuti + minutiTrajanje);
			datumKrajaProjekcije = datumKrajaProjekcije.toString();
			/*console.log(datumPocetkaProjekcije);
			console.log(datumKrajaProjekcije);*/
			var sadasnjiDatum = new Date();
			sadasnjiDatum = sadasnjiDatum.toString();
			/*console.log(sadasnjiDatum);*/

			if (new Date (datumPocetkaProjekcije) > new Date (sadasnjiDatum) == false) {
				window.alert("Nije moguće da uneti datum bude odabran za početak prikazivanja nove projekcije.");
				return;
			}

			// Preklapanje datuma projekcija!
			var projekcije = JSON.parse(localStorage.getItem("projekcije"));
			for (let i = 0; i < projekcije.length; i++) {
				if (sala == projekcije[i]["sala"]) {
					var datumPostojeceProjekcije = projekcije[i]["pocetakProjekcije"].split("|");
					/*console.log(datumPostojeceProjekcije);*/
					var godinaPostojeceProjekcije = parseInt(datumPostojeceProjekcije[0]);
					var mesecPostojeceProjekcije = parseInt(datumPostojeceProjekcije[1]);
					var danPostojeceProjekcije = parseInt(datumPostojeceProjekcije[2]);
					var satiPostojeceProjekcije = parseInt(datumPostojeceProjekcije[3]);
					var minutiPostojeceProjekcije = parseInt(datumPostojeceProjekcije[4]);
					datumPostojeceProjekcije = new Date (godinaPostojeceProjekcije, mesecPostojeceProjekcije, 
						danPostojeceProjekcije, satiPostojeceProjekcije, minutiPostojeceProjekcije);
					var trajanjePostojeceProjekcije = projekcije[i]["duzinaProjekcije"].split("|");
					var satiTrajanjaPostojeceProjekcije = parseInt(trajanjePostojeceProjekcije[0]);
					var minutiTrajanjaPostojeceProjekcije = parseInt(trajanjePostojeceProjekcije[1]);
					var krajPostojeceProjekcije = new Date(godinaPostojeceProjekcije, mesecPostojeceProjekcije, 
						danPostojeceProjekcije, satiPostojeceProjekcije + satiTrajanjaPostojeceProjekcije, 
						minutiPostojeceProjekcije + minutiTrajanjaPostojeceProjekcije);

					datumPostojeceProjekcije = datumPostojeceProjekcije.toString();
					krajPostojeceProjekcije = krajPostojeceProjekcije.toString();

					if (datumPocetkaProjekcije < datumPostojeceProjekcije && datumKrajaProjekcije > datumPostojeceProjekcije) {
						window.alert("Trenutno postoji projekcija koja se emituje tog datuma u unetom intervalu.");
						return;
					} 

					if (datumPocetkaProjekcije < krajPostojeceProjekcije && datumKrajaProjekcije > krajPostojeceProjekcije) {
						window.alert("Trenutno postoji projekcija koja se emituje tog datuma u unetom intervalu.");
						return;
					}

					/*console.log(datumPostojeceProjekcije);
					console.log(krajPostojeceProjekcije);*/
				}

			}
			

			var cena = document.getElementById("formaZaCenuNoveProjekcijeInput").value;
			/*console.log(cena);*/
			if (cena.isNumber() == true) {
				window.alert("Za unos cene projekcije koristite brojeve.");
				return;
			}

			if (cena == "") {
				window.alert("Unesite cenu projekcije.");
				return;
			}

			cena = parseInt(cena);

			if (cena < 100) {
				window.alert("Najniža cena projekcije je 100 dinara.");
				return;
			}

			cena = cena.toString();


			var sale = JSON.parse(localStorage.getItem("sale"));
			for (let i = 0; i < sale.length; i++) {
				if (sala == sale[i]["idSale"]) {
					var brojMestaUSali = sale[i]["brojMestaUSali"];
				}
			}

			for (let i = 0; i < filmovi.length; i++) {
				if (filmovi[i]["naziv"] == nazivFilma) {
					var zanrFilma = filmovi[i]["zanr"];
				}
			}

			projekcije[i]["pocetakProjekcije"] = pocetakProjekcije;
			projekcije[i]["duzinaProjekcije"] = trajanje;
			projekcije[i]["cena"] = cena;
			projekcije[i]["nazivFilma"] = nazivFilma;
			projekcije[i]["trailerZaFilm"] = trailerZaFilm;
			projekcije[i]["zanrFilma"] = zanrFilma;
			projekcije[i]["sala"] = sala;
			projekcije[i]["brojSlobodnihMesta"] = brojMestaUSali;
			projekcije[i]["brojMestaUSali"] = brojMestaUSali;
		}
	}

	localStorage.setItem("projekcije", JSON.stringify(projekcije));

	window.alert("Projekcija je uspešno izmenjena.");
	document.getElementById("petiRedP4").style.display = "block";
	document.getElementById("petiRedP4").style.cursor = "pointer";

	location.reload("projekcije.html"); 
}

function brisanjeProjekcije(idProjekcije) {
	var projekcije = localStorage.getItem("projekcije");
	projekcije = JSON.parse(projekcije);
	
	for (let i = 0; i < projekcije.length; i ++) {
		if (projekcije[i]["idProjekcije"] == idProjekcije) {
			projekcije[i]["obrisana"] = true;
			/*obrisanaProjekcija = projekcije[i];*/
		}
	}

	localStorage.setItem("projekcije", JSON.stringify(projekcije));

	window.alert("Projekcija je obrisana.");
	location.reload("projekcije.html"); 
}

function prikaziSamoVaseOpcije() {
	document.getElementById("drugiRed").style.display = "none";
	document.getElementById("treciRed").style.display = "none";
	document.getElementById("petiRedDiv1").style.display = "none";
	document.getElementById("petiRedDiv2").style.display = "none";

	document.getElementById("petiRedP2").style.cursor = "pointer";
	document.getElementById("petiRedP3").style.cursor = "pointer";
	document.getElementById("petiRedP4").style.cursor = "pointer";

	document.getElementById("cetvrtiRedPrikazPretrage").style.display = "none";

	isprazniStorageeZaPretraguProjekcija();
}

var isPrikaziDodatneOpcije = false;
function prikaziDodatneOpcije() {
	document.getElementById("petiRedDiv1").style.display = "block";
	/*divZaVaseOpcije.children[3] = divZaVaseOpcije.children[4]; 
	divZaVaseOpcije.children[2] = divZaVaseOpcije.children[3]; */
	var pretragaProjekcija = document.getElementById("petiRedDiv1");

	if (isPrikaziDodatneOpcije == false) {
		var listaPretragaProjekcija = document.createElement("ul");
		var PretragaPremaIDju = document.createElement("li");
		var tekstPretragaPremaIDju = document.createTextNode(" Pretraga prema ID-ju ");
		PretragaPremaIDju.appendChild(tekstPretragaPremaIDju);
		PretragaPremaIDju.setAttribute("style", "cursor:pointer");
		PretragaPremaIDju.setAttribute("onclick", "prikazPretragePremaIDju()");
		var PretragaPremaNazivuFilma = document.createElement("li");
		var tekstPretragaPremaNazivuFilma = document.createTextNode(" Pretraga prema nazivu filma ");
		PretragaPremaNazivuFilma.appendChild(tekstPretragaPremaNazivuFilma);
		PretragaPremaNazivuFilma.setAttribute("style", "cursor:pointer");
		PretragaPremaNazivuFilma.setAttribute("onclick", "prikazPretragePremaNazivuFilma()");
		var PretragaPremaŽanruFilma = document.createElement("li");
		var tekstPretragaPremaŽanruFilma = document.createTextNode(" Pretraga prema žanru filma ");
		PretragaPremaŽanruFilma.appendChild(tekstPretragaPremaŽanruFilma);
		PretragaPremaŽanruFilma.setAttribute("style", "cursor:pointer");
		PretragaPremaŽanruFilma.setAttribute("onclick", "prikazPretragePremaŽanruFilma()");
		var PretragaPremaSali = document.createElement("li");
		var tekstPretragaPremaSali = document.createTextNode(" Pretraga prema sali ");
		PretragaPremaSali.appendChild(tekstPretragaPremaSali);
		PretragaPremaSali.setAttribute("style", "cursor:pointer");
		PretragaPremaSali.setAttribute("onclick", "prikazPretragePremaSali()");
		listaPretragaProjekcija.appendChild(PretragaPremaIDju);
		listaPretragaProjekcija.appendChild(PretragaPremaNazivuFilma);
		listaPretragaProjekcija.appendChild(PretragaPremaŽanruFilma);
		listaPretragaProjekcija.appendChild(PretragaPremaSali);

		pretragaProjekcija.appendChild(listaPretragaProjekcija);
	}
	isPrikaziDodatneOpcije = true;
	return isPrikaziDodatneOpcije;
}

function prikazPretragePremaIDju() {

	isprazniStorageeZaPretraguProjekcija();

	document.getElementById("petiRedDiv2").style.display = "block";
	document.getElementById("PPretragaProjekcijePremaNazivuFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaZanruFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaSaliFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePoID").style.display = "block";
}

function prikazPretragePremaNazivuFilma() {

	isprazniStorageeZaPretraguProjekcija();

	document.getElementById("petiRedDiv2").style.display = "block";
	document.getElementById("PPretragaProjekcijePoID").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaZanruFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaSaliFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaNazivuFilma").style.display = "block";
}

function prikazPretragePremaŽanruFilma() {

	isprazniStorageeZaPretraguProjekcija();

	document.getElementById("petiRedDiv2").style.display = "block";
	document.getElementById("PPretragaProjekcijePoID").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaNazivuFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaSaliFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaZanruFilma").style.display = "block";
}

function prikazPretragePremaSali() {

	isprazniStorageeZaPretraguProjekcija();
	
	document.getElementById("petiRedDiv2").style.display = "block";
	document.getElementById("PPretragaProjekcijePoID").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaNazivuFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaZanruFilma").style.display = "none";
	document.getElementById("PPretragaProjekcijePremaSaliFilma").style.display = "block";
}

String.prototype.toTitleCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function izvrsiPretraguProjekcija() {
	var PPretragaProjekcijePoIDDisplay = document.getElementById("PPretragaProjekcijePoID").style.display;
	var PPretragaProjekcijePremaNazivuFilmaDisplay = document.getElementById("PPretragaProjekcijePremaNazivuFilma").style.display;
	var PPretragaProjekcijePremaZanruFilmaDisplay = document.getElementById("PPretragaProjekcijePremaZanruFilma").style.display;
	var PPretragaProjekcijePremaSaliFilmaDisplay = document.getElementById("PPretragaProjekcijePremaSaliFilma").style.display;

	var unosPretrage = document.getElementById("inputZaPretraguProjekcija").value;

	if (PPretragaProjekcijePoIDDisplay == "block") {
		izvrsiPretraguProjekcijaPoIDju();
	}
	

	if (PPretragaProjekcijePremaNazivuFilmaDisplay == "block") {
		izvrsiPretraguProjekcijaPoNazivuFilma();
	}

	if (PPretragaProjekcijePremaZanruFilmaDisplay == "block") {
		izvrsiPretraguProjekcijaPoZanruFilma();
	}

	if (PPretragaProjekcijePremaSaliFilmaDisplay == "block") {
		izvrsiPretraguProjekcijaPoSaliFilma();
	}
}

function izvrsiPretraguProjekcijaPoIDju() {
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	var trazeneProjekcijePoIDju = JSON.parse(localStorage.getItem("trazeneProjekcijePoIDju"));
	var unosPretrage = document.getElementById("inputZaPretraguProjekcija").value;

	document.getElementById("cetvrtiRedPrikazPretrage").style.display = "block"
		
	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
		
	if (unosPretrage.isNumber() == true) {
		window.alert("Za pretragu po ID-ju koristite brojeve.");
		return;
	}
		
	if (unosPretrage == "") {
		window.alert("Unesite ID projekcije.");
		return;
	}
	/*console.log(projekcije);
	console.log(typeof unosPretrage);*/
	var uradiPrikazivanjeProjekcija = false;
	var pronadjenaProjekcija = false;
	var postojiUProjekcijama = false;
	for (let i = 0; i < projekcije.length; i++) {
		/*console.log(projekcije[i]["idProjekcije"]);*/
		if (projekcije[i]["idProjekcije"] == unosPretrage && projekcije[i]["obrisana"] == false) {
			for (let j = 0; j < trazeneProjekcijePoIDju.length; j++) {
				if (unosPretrage == trazeneProjekcijePoIDju[j]["idProjekcije"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoIDju.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoIDju", JSON.stringify(trazeneProjekcijePoIDju));			
			}
			postojiUProjekcijama = true;
		}
	}

	if (!postojiUProjekcijama) {
		window.alert("Projekcija sa unetim ID-jem ne postoji.");
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
			
	if (!uradiPrikazivanjeProjekcija) {

		var cetvrtiRedPrikazPretrage = document.getElementById("cetvrtiRedPrikazPretrage");
		cetvrtiRedPrikazPretrage.removeChild(cetvrtiRedPrikazPretrage.childNodes[1]);
		var cetvrtiRedPrikazPretrageDiv = document.createElement("div");
		cetvrtiRedPrikazPretrageDiv.setAttribute("id", "cetvrtiRedPrikazPretrageDiv")
		cetvrtiRedPrikazPretrage.appendChild(cetvrtiRedPrikazPretrageDiv);

		for (let i = 1; i < trazeneProjekcijePoIDju.length; i++) {

			var nazivFilmaPoIDju = trazeneProjekcijePoIDju[i]["nazivFilma"];
			var pocetakProjekcijePoIDju = trazeneProjekcijePoIDju[i]["pocetakProjekcije"];
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.split("|");
			var pocetakProjekcijePoIDjuGodina = pocetakProjekcijePoIDju[0];
			var pocetakProjekcijePoIDjuMesec = pocetakProjekcijePoIDju[1];
			var pocetakProjekcijePoIDjuDan = pocetakProjekcijePoIDju[2];
			var pocetakProjekcijePoIDjuSati = pocetakProjekcijePoIDju[3];
			var pocetakProjekcijePoIDjuMinuti = pocetakProjekcijePoIDju[4];
			pocetakProjekcijePoIDju = new Date(pocetakProjekcijePoIDjuGodina, pocetakProjekcijePoIDjuMesec, pocetakProjekcijePoIDjuDan, 
				pocetakProjekcijePoIDjuSati, pocetakProjekcijePoIDjuMinuti);
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.toString();
			var cenaProjekcijePoIDju = trazeneProjekcijePoIDju[i]["cena"];


			var div = document.getElementById("cetvrtiRedPrikazPretrageDiv");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekciju");
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilmaPoIDju);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcijePoIDju);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcijePoIDju + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuKupiKartu = document.createElement("div");
			divZaProjekcijuKupiKartu.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuKupiKartuParagraf = document.createElement("p");
			divZaProjekcijuKupiKartuParagraf.setAttribute("style", "cursor:pointer; text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuKupiKartu = document.createTextNode("Kupi kartu");
			divZaProjekcijuKupiKartuParagraf.appendChild(tekstDivZaProjekcijuKupiKartu);
			divZaProjekcijuKupiKartu.appendChild(divZaProjekcijuKupiKartuParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);

			/*if (aktivniKorisnik["uloga"] == "prodavac") {
				divZaProjekciju.appendChild(divZaProjekcijuKupiKartu);
			}*/

			div.appendChild(divZaProjekciju);
		}
	}
}

function izvrsiPretraguProjekcijaPoNazivuFilma() {
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	var trazeneProjekcijePoNazivuFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoNazivuFilma"));
	var unosPretrage = document.getElementById("inputZaPretraguProjekcija").value;

	document.getElementById("cetvrtiRedPrikazPretrage").style.display = "block"
	
	if (unosPretrage == "") {
		window.alert("Unesite ime filma po kojem želite da se izvrši pretraga.");
		return;
	}

	var uradiPrikazivanjeProjekcija = false;
	var pronadjenaProjekcija = false;
	var postojiUProjekcijama = false;
	for (let i = 0; i < projekcije.length; i++) {
		/*console.log(projekcije[i]["nazivFilma"]);*/
		if (projekcije[i]["nazivFilma"] == unosPretrage && projekcije[i]["obrisana"] == false) {
			for (let j = 0; j < trazeneProjekcijePoNazivuFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoNazivuFilma[j]["nazivFilma"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoNazivuFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));			
			}
			postojiUProjekcijama = true;
		}
		var reciUNazivuFilma = projekcije[i]["nazivFilma"].split(" ");

		for (let j = 0; j < reciUNazivuFilma.length; j++) {
			if (unosPretrage == reciUNazivuFilma[j] && projekcije[i]["obrisana"] == false) {
				unosPretrage = projekcije[i]["nazivFilma"];
				for (let k = 0; k < trazeneProjekcijePoNazivuFilma.length; k++) {
					if (unosPretrage == trazeneProjekcijePoNazivuFilma[k]["nazivFilma"]) {
						pronadjenaProjekcija = true;
						uradiPrikazivanjeProjekcija = true;
					}
				}
				if (!pronadjenaProjekcija) {
					trazeneProjekcijePoNazivuFilma.push(projekcije[i]);
					localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));
				}
				postojiUProjekcijama = true;
			}
			
		}

		for (let j = 0; j < reciUNazivuFilma.length; j++) {
			if (unosPretrage == reciUNazivuFilma[j].toLowerCase() && projekcije[i]["obrisana"] == false) {
				unosPretrage = projekcije[i]["nazivFilma"];
				for (let k = 0; k < trazeneProjekcijePoNazivuFilma.length; k++) {
					if (unosPretrage == trazeneProjekcijePoNazivuFilma[k]["nazivFilma"]) {
						pronadjenaProjekcija = true;
						uradiPrikazivanjeProjekcija = true;
					}
				}
				if (!pronadjenaProjekcija) {
					trazeneProjekcijePoNazivuFilma.push(projekcije[i]);
					localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));
				}
				postojiUProjekcijama = true;
			}
			
		}

		for (let j = 0; j < reciUNazivuFilma.length; j++) {
			if (unosPretrage == reciUNazivuFilma[j].toUpperCase() && projekcije[i]["obrisana"] == false) {
				unosPretrage = projekcije[i]["nazivFilma"];
				for (let k = 0; k < trazeneProjekcijePoNazivuFilma.length; k++) {
					if (unosPretrage == trazeneProjekcijePoNazivuFilma[k]["nazivFilma"]) {
						pronadjenaProjekcija = true;
						uradiPrikazivanjeProjekcija = true;
					}
				}
				if (!pronadjenaProjekcija) {
					trazeneProjekcijePoNazivuFilma.push(projekcije[i]);
					localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));
				}
				postojiUProjekcijama = true;
			}
			
		}

	}

	if (!postojiUProjekcijama) {
		window.alert("Projekcija sa unetim nazivom filma ne postoji.");
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);

	if (!uradiPrikazivanjeProjekcija) {

		var cetvrtiRedPrikazPretrage = document.getElementById("cetvrtiRedPrikazPretrage");
		cetvrtiRedPrikazPretrage.removeChild(cetvrtiRedPrikazPretrage.childNodes[1]);
		var cetvrtiRedPrikazPretrageDiv = document.createElement("div");
		cetvrtiRedPrikazPretrageDiv.setAttribute("id", "cetvrtiRedPrikazPretrageDiv")
		cetvrtiRedPrikazPretrage.appendChild(cetvrtiRedPrikazPretrageDiv);

		for (let i = 1; i < trazeneProjekcijePoNazivuFilma.length; i++) {

			var nazivFilmaPoIDju = trazeneProjekcijePoNazivuFilma[i]["nazivFilma"];
			var pocetakProjekcijePoIDju = trazeneProjekcijePoNazivuFilma[i]["pocetakProjekcije"];
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.split("|");
			var pocetakProjekcijePoIDjuGodina = pocetakProjekcijePoIDju[0];
			var pocetakProjekcijePoIDjuMesec = pocetakProjekcijePoIDju[1];
			var pocetakProjekcijePoIDjuDan = pocetakProjekcijePoIDju[2];
			var pocetakProjekcijePoIDjuSati = pocetakProjekcijePoIDju[3];
			var pocetakProjekcijePoIDjuMinuti = pocetakProjekcijePoIDju[4];
			pocetakProjekcijePoIDju = new Date(pocetakProjekcijePoIDjuGodina, pocetakProjekcijePoIDjuMesec, pocetakProjekcijePoIDjuDan, 
				pocetakProjekcijePoIDjuSati, pocetakProjekcijePoIDjuMinuti);
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.toString();
			var cenaProjekcijePoIDju = trazeneProjekcijePoNazivuFilma[i]["cena"];


			var div = document.getElementById("cetvrtiRedPrikazPretrageDiv");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekciju");
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilmaPoIDju);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcijePoIDju);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcijePoIDju + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuKupiKartu = document.createElement("div");
			divZaProjekcijuKupiKartu.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuKupiKartuParagraf = document.createElement("p");
			divZaProjekcijuKupiKartuParagraf.setAttribute("style", "cursor:pointer; text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuKupiKartu = document.createTextNode("Kupi kartu");
			divZaProjekcijuKupiKartuParagraf.appendChild(tekstDivZaProjekcijuKupiKartu);
			divZaProjekcijuKupiKartu.appendChild(divZaProjekcijuKupiKartuParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);

			/*if (aktivniKorisnik["uloga"] == "prodavac") {
				divZaProjekciju.appendChild(divZaProjekcijuKupiKartu);
			}*/

			div.appendChild(divZaProjekciju);
		}
	}
}

function izvrsiPretraguProjekcijaPoZanruFilma() {
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	var trazeneProjekcijePoZanruFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoZanruFilma"));
	var unosPretrage = document.getElementById("inputZaPretraguProjekcija").value;

	document.getElementById("cetvrtiRedPrikazPretrage").style.display = "block"
		
	String.prototype.isNumber = function(){return /[^0-9]/.test(this);}
		
	if (unosPretrage.isNumber() == false) {
		window.alert("Za pretragu po žanru filma ne koristite brojeve.");
		return;
	}
		
	if (unosPretrage == "") {
		window.alert("Unesite žanr filma.");
		return;
	}
	/*console.log(projekcije);
	console.log(typeof unosPretrage);*/
	var uradiPrikazivanjeProjekcija = false;
	var pronadjenaProjekcija = false;
	var postojiUProjekcijama = false;
	for (let i = 0; i < projekcije.length; i++) {
		/*console.log(projekcije[i]["zanrFilma"]);*/
		if (projekcije[i]["zanrFilma"] == unosPretrage && projekcije[i]["obrisana"] == false) {
			unosPretrage = projekcije[i]["zanrFilma"];
			for (let j = 0; j < trazeneProjekcijePoZanruFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoZanruFilma[j]["zanrFilma"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoZanruFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoZanruFilma", JSON.stringify(trazeneProjekcijePoZanruFilma));			
			}
			postojiUProjekcijama = true;
		}

		if (projekcije[i]["zanrFilma"].toUpperCase() == unosPretrage && projekcije[i]["obrisana"] == false) {
			unosPretrage = projekcije[i]["zanrFilma"];
			for (let j = 0; j < trazeneProjekcijePoZanruFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoZanruFilma[j]["zanrFilma"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoZanruFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoZanruFilma", JSON.stringify(trazeneProjekcijePoZanruFilma));			
			}
			postojiUProjekcijama = true;
		}

		if (projekcije[i]["zanrFilma"].toTitleCase() == unosPretrage && projekcije[i]["obrisana"] == false) {
			unosPretrage = projekcije[i]["zanrFilma"];
			for (let j = 0; j < trazeneProjekcijePoZanruFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoZanruFilma[j]["zanrFilma"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoZanruFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoZanruFilma", JSON.stringify(trazeneProjekcijePoZanruFilma));			
			}
			postojiUProjekcijama = true;
		}
	}

	if (!postojiUProjekcijama) {
		window.alert("Projekcija sa unetim žanrom filma ne postoji.");
	}

	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);
			
	if (!uradiPrikazivanjeProjekcija) {

		var cetvrtiRedPrikazPretrage = document.getElementById("cetvrtiRedPrikazPretrage");
		cetvrtiRedPrikazPretrage.removeChild(cetvrtiRedPrikazPretrage.childNodes[1]);
		var cetvrtiRedPrikazPretrageDiv = document.createElement("div");
		cetvrtiRedPrikazPretrageDiv.setAttribute("id", "cetvrtiRedPrikazPretrageDiv")
		cetvrtiRedPrikazPretrage.appendChild(cetvrtiRedPrikazPretrageDiv);

		for (let i = 1; i < trazeneProjekcijePoZanruFilma.length; i++) {

			var nazivFilmaPoIDju = trazeneProjekcijePoZanruFilma[i]["nazivFilma"];
			var pocetakProjekcijePoIDju = trazeneProjekcijePoZanruFilma[i]["pocetakProjekcije"];
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.split("|");
			var pocetakProjekcijePoIDjuGodina = pocetakProjekcijePoIDju[0];
			var pocetakProjekcijePoIDjuMesec = pocetakProjekcijePoIDju[1];
			var pocetakProjekcijePoIDjuDan = pocetakProjekcijePoIDju[2];
			var pocetakProjekcijePoIDjuSati = pocetakProjekcijePoIDju[3];
			var pocetakProjekcijePoIDjuMinuti = pocetakProjekcijePoIDju[4];
			pocetakProjekcijePoIDju = new Date(pocetakProjekcijePoIDjuGodina, pocetakProjekcijePoIDjuMesec, pocetakProjekcijePoIDjuDan, 
				pocetakProjekcijePoIDjuSati, pocetakProjekcijePoIDjuMinuti);
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.toString();
			var cenaProjekcijePoIDju = trazeneProjekcijePoZanruFilma[i]["cena"];


			var div = document.getElementById("cetvrtiRedPrikazPretrageDiv");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekciju");
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilmaPoIDju);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcijePoIDju);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcijePoIDju + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuKupiKartu = document.createElement("div");
			divZaProjekcijuKupiKartu.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuKupiKartuParagraf = document.createElement("p");
			divZaProjekcijuKupiKartuParagraf.setAttribute("style", "cursor:pointer; text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuKupiKartu = document.createTextNode("Kupi kartu");
			divZaProjekcijuKupiKartuParagraf.appendChild(tekstDivZaProjekcijuKupiKartu);
			divZaProjekcijuKupiKartu.appendChild(divZaProjekcijuKupiKartuParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);

			/*if (aktivniKorisnik["uloga"] == "prodavac") {
				divZaProjekciju.appendChild(divZaProjekcijuKupiKartu);
			}*/

			div.appendChild(divZaProjekciju);
		}
	}
}

function izvrsiPretraguProjekcijaPoSaliFilma() {
	var projekcije = JSON.parse(localStorage.getItem("projekcije"));
	var trazeneProjekcijePoSaliFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoSaliFilma"));
	var unosPretrage = document.getElementById("inputZaPretraguProjekcija").value;

	document.getElementById("cetvrtiRedPrikazPretrage").style.display = "block"
		
	if (unosPretrage == "") {
		window.alert("Unesite salu za pretragu filma.");
		return;
	}
	/*console.log(projekcije);
	console.log(typeof unosPretrage);*/
	var uradiPrikazivanjeProjekcija = false;
	var pronadjenaProjekcija = false;
	var postojiUProjekcijama = false;
	for (let i = 0; i < projekcije.length; i++) {
		if (projekcije[i]["sala"] == unosPretrage && projekcije[i]["obrisana"] == false) {
			unosPretrage = projekcije[i]["sala"];
			for (let j = 0; j < trazeneProjekcijePoSaliFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoSaliFilma[j]["sala"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoSaliFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoSaliFilma", JSON.stringify(trazeneProjekcijePoSaliFilma));			
			}
			postojiUProjekcijama = true;
		}

		if (projekcije[i]["sala"].toLowerCase() == unosPretrage && projekcije[i]["obrisana"] == false) {
			unosPretrage = projekcije[i]["sala"];
			for (let j = 0; j < trazeneProjekcijePoSaliFilma.length; j++) {
				if (unosPretrage == trazeneProjekcijePoSaliFilma[j]["sala"]) {
					pronadjenaProjekcija = true;
					uradiPrikazivanjeProjekcija = true;
				}
			}
			if (!pronadjenaProjekcija) {
				trazeneProjekcijePoSaliFilma.push(projekcije[i]);
				localStorage.setItem("trazeneProjekcijePoSaliFilma", JSON.stringify(trazeneProjekcijePoSaliFilma));			
			}
			postojiUProjekcijama = true;
		}
	}

	if (!postojiUProjekcijama) {
		window.alert("Projekcija sa unetom salom filma ne postoji.");
	}
	
	var aktivniKorisnik = localStorage.getItem("aktivniKorisnik");
	aktivniKorisnik = JSON.parse(aktivniKorisnik);

	if (!uradiPrikazivanjeProjekcija) {

		var cetvrtiRedPrikazPretrage = document.getElementById("cetvrtiRedPrikazPretrage");
		cetvrtiRedPrikazPretrage.removeChild(cetvrtiRedPrikazPretrage.childNodes[1]);
		var cetvrtiRedPrikazPretrageDiv = document.createElement("div");
		cetvrtiRedPrikazPretrageDiv.setAttribute("id", "cetvrtiRedPrikazPretrageDiv")
		cetvrtiRedPrikazPretrage.appendChild(cetvrtiRedPrikazPretrageDiv);

		for (let i = 1; i < trazeneProjekcijePoSaliFilma.length; i++) {

			var nazivFilmaPoIDju = trazeneProjekcijePoSaliFilma[i]["nazivFilma"];
			var pocetakProjekcijePoIDju = trazeneProjekcijePoSaliFilma[i]["pocetakProjekcije"];
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.split("|");
			var pocetakProjekcijePoIDjuGodina = pocetakProjekcijePoIDju[0];
			var pocetakProjekcijePoIDjuMesec = pocetakProjekcijePoIDju[1];
			var pocetakProjekcijePoIDjuDan = pocetakProjekcijePoIDju[2];
			var pocetakProjekcijePoIDjuSati = pocetakProjekcijePoIDju[3];
			var pocetakProjekcijePoIDjuMinuti = pocetakProjekcijePoIDju[4];
			pocetakProjekcijePoIDju = new Date(pocetakProjekcijePoIDjuGodina, pocetakProjekcijePoIDjuMesec, pocetakProjekcijePoIDjuDan, 
				pocetakProjekcijePoIDjuSati, pocetakProjekcijePoIDjuMinuti);
			pocetakProjekcijePoIDju = pocetakProjekcijePoIDju.toString();
			var cenaProjekcijePoIDju = trazeneProjekcijePoSaliFilma[i]["cena"];


			var div = document.getElementById("cetvrtiRedPrikazPretrageDiv");
			var divZaProjekciju = document.createElement("div");
			divZaProjekciju.setAttribute("style", "width:100%; border-top:1.5px dotted black;");
			divZaProjekciju.setAttribute("id", "divZaProjekciju");
			var divZaProjekcijuNaziv = document.createElement("div");
			divZaProjekcijuNaziv.setAttribute("style", "position:relative; width:100%; height: auto; border-bottom: 1px solid black; box-shadow: 0px 10px 10px #888888; border-radius: 10px;");
			var divZaProjekcijuNazivParagraf = document.createElement("p");
			divZaProjekcijuNazivParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuNaziv = document.createTextNode("Naziv filma: " + nazivFilmaPoIDju);
			divZaProjekcijuNazivParagraf.appendChild(tekstDivZaProjekcijuNaziv);
			divZaProjekcijuNaziv.appendChild(divZaProjekcijuNazivParagraf);
			var divZaProjekcijuDatum = document.createElement("div");
			divZaProjekcijuDatum.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuDatumParagraf = document.createElement("p");
			divZaProjekcijuDatumParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuDatum = document.createTextNode("Datum prikazivanja projekcije: " + pocetakProjekcijePoIDju);
			divZaProjekcijuDatumParagraf.appendChild(tekstDivZaProjekcijuDatum);
			divZaProjekcijuDatum.appendChild(divZaProjekcijuDatumParagraf);
			var divZaProjekcijuCena = document.createElement("div");
			divZaProjekcijuCena.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuCenaParagraf = document.createElement("p");
			divZaProjekcijuCenaParagraf.setAttribute("style", "text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuCena = document.createTextNode("Cena karte: " + cenaProjekcijePoIDju + " dinara");
			divZaProjekcijuCenaParagraf.appendChild(tekstDivZaProjekcijuCena);
			divZaProjekcijuCena.appendChild(divZaProjekcijuCenaParagraf);
			var divZaProjekcijuKupiKartu = document.createElement("div");
			divZaProjekcijuKupiKartu.setAttribute("style", "position:relative; width:100%; height: auto; border-radius: 10px;");
			var divZaProjekcijuKupiKartuParagraf = document.createElement("p");
			divZaProjekcijuKupiKartuParagraf.setAttribute("style", "cursor:pointer; text-align:center; font-family:trebuchet ms; font-weight: bold;");
			var tekstDivZaProjekcijuKupiKartu = document.createTextNode("Kupi kartu");
			divZaProjekcijuKupiKartuParagraf.appendChild(tekstDivZaProjekcijuKupiKartu);
			divZaProjekcijuKupiKartu.appendChild(divZaProjekcijuKupiKartuParagraf);

			divZaProjekciju.appendChild(divZaProjekcijuNaziv);
			divZaProjekciju.appendChild(divZaProjekcijuDatum);
			divZaProjekciju.appendChild(divZaProjekcijuCena);

			/*if (aktivniKorisnik["uloga"] == "prodavac") {
				divZaProjekciju.appendChild(divZaProjekcijuKupiKartu);
			}*/

			div.appendChild(divZaProjekciju);
		}
	}
}

function isprazniStorageeZaPretraguProjekcija() {

	var cetvrtiRedPrikazPretrage = document.getElementById("cetvrtiRedPrikazPretrage");
	cetvrtiRedPrikazPretrage.removeChild(cetvrtiRedPrikazPretrage.childNodes[1]);
	var cetvrtiRedPrikazPretrageDiv = document.createElement("div");
	cetvrtiRedPrikazPretrageDiv.setAttribute("id", "cetvrtiRedPrikazPretrageDiv")
	cetvrtiRedPrikazPretrage.appendChild(cetvrtiRedPrikazPretrageDiv);
	
	var trazeneProjekcijePoIDju = JSON.parse(localStorage.getItem("trazeneProjekcijePoIDju"));
	trazeneProjekcijePoIDju = [{"idProjekcije": ""}];
	localStorage.setItem("trazeneProjekcijePoIDju", JSON.stringify(trazeneProjekcijePoIDju));

	var trazeneProjekcijePoNazivuFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoNazivuFilma"));
	trazeneProjekcijePoNazivuFilma = [{"nazivFilma": ""}];
	localStorage.setItem("trazeneProjekcijePoNazivuFilma", JSON.stringify(trazeneProjekcijePoNazivuFilma));

	var trazeneProjekcijePoZanruFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoZanruFilma"));
	trazeneProjekcijePoZanruFilma = [{"zanrFilma": ""}];
	localStorage.setItem("trazeneProjekcijePoZanruFilma", JSON.stringify(trazeneProjekcijePoZanruFilma));

	var trazeneProjekcijePoSaliFilma = JSON.parse(localStorage.getItem("trazeneProjekcijePoSaliFilma"));
	trazeneProjekcijePoSaliFilma = [{"sala": ""}];
	localStorage.setItem("trazeneProjekcijePoSaliFilma", JSON.stringify(trazeneProjekcijePoSaliFilma));
}

function windowAlertZaKontakt() {
	window.alert("Kontakt telefon: 021/495-652.")
}